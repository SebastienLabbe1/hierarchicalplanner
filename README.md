Welcome to an implementattion of a hierarchical planner using space decomposition and parallel local planning.

This project uses the following libraries :
fcl
ompl
eigen

usefull commands:
./run.sh pexample 

this will:
- make
- run 
- openvisualizer
- print solution lengths

the problems are definied in data/problems, to look at the layout see pexample

the variables in data/metavariables are very important and can change the efficiency drasticaly

depending on problem and path wanted it may also be a good idea to modify the < operator for job structs this is in HierarchicalPlanner.cpp

