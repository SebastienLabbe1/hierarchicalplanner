#include "Environment.h"
#include "FaceNeigh.h"
#include "Plane.h"
#include "utils.h"

#include <eigen3/Eigen/Dense>
#include <fstream>
#include <time.h>

#define EPSE 1.0e-10

//TODO
//assert that obstacles are disjoint or treat this case
//assert obstacles are in workspace
//assert or treat convexity


Environment::Environment(Mesh &workspace, std::vector<Mesh> &obstacles) {
	this->workspace = workspace;
	this->obstacles = obstacles;
}

Environment::~Environment(void) {
}

void Environment::divideWorkspace() {
	//Partitioning of the workspace

	//Preparing vertecies, faces and cuts(obstacle triangles)
	wverts = workspace.verts;
	wfaces = workspace.faces;
	int siP = newPartition();

	Mesh m;
	for(auto obstacle : obstacles) m.merge(&obstacle);

	std::vector<Eigen::Matrix<double, 3, 1>> cverts = m.verts;
	std::vector<std::vector<int>> cfaces = m.faces;
	std::vector<Eigen::Matrix<double, 3, 1>> cut_centers;
	std::vector<int> cut_center_indexes;
	for(auto obstacle : obstacles) {
		Eigen::Matrix<double, 3, 1> minn = obstacle.verts[obstacle.faces[0][0]];
		Eigen::Matrix<double, 3, 1> maxx = minn;
		for(auto face : obstacle.faces) {
			for(int iV : face) {
				minn = minn.cwiseMin(obstacle.verts[iV]);
				maxx = maxx.cwiseMax(obstacle.verts[iV]);
			}
		}
		Eigen::Matrix<double, 3, 1> center = (minn + maxx) / 2;
		for(auto face : obstacle.faces) cut_center_indexes.push_back(cut_centers.size());
		cut_centers.push_back(center);
	}

	//creating one partition with all cuts and all faces
	std::vector<std::vector<int>> partition_cuts(1);
	for(int i = 0; i < int(cfaces.size()); i ++) partition_cuts[0].push_back(i);
	for(int i = 0; i < int(wfaces.size()); i ++) partition_faces[siP].push_back(i);

	//Data sturcture to keep the connectivity of the of the so called tree
	FaceNeigh fneighs = FaceNeigh(wfaces.size());

	//Queue to keep partitions that still need to be modified
	std::queue<int> part_queue;
	part_queue.push(siP);

	//Store partition ids which are obstacles
	std::vector<int> filled_partitions;

	while(!part_queue.empty()) {
		//Chose the partition to modify
		int iP = part_queue.front();
		part_queue.pop();

		if(!partition_cuts[iP].size()) {
			setPartitionBounds(iP);
			auto diag = partition_bound[iP].second - partition_bound[iP].first;
			if(diag.minCoeff() < EPSE) continue;

			//If the partition has one side longer than partitionMaxSize
			//create a new cut and cut it again
			for(int i = 0; i < 3; i ++) {
				if(diag[i] > partitionMaxSize) {
					int icV = cverts.size();
					int icF = cfaces.size();
					Eigen::Matrix<double, 3, 1> v;
					double r;
					for(int j = 0; j < 3; j ++) {
						v = partition_bound[iP].first;
						for(int k = 0; k < 3; k ++) {
							if(i == k) r = 0.5;
							else if(j == k) r = 0.33333;
							else r = 0.66666;
							v(k, 0) += r * diag[k];
						}
						cverts.push_back(v);
					}
					cfaces.push_back({icV, icV+1, icV+2});
					partition_cuts[iP].push_back(icF);
					cut_center_indexes.push_back(-1);
					break;
				}
			}
			if(!partition_cuts[iP].size()) continue;
		}


		//Seting up the plane
		int iC = partition_cuts[iP][0];
		std::vector<Eigen::Matrix<double, 3, 1>> V;
		for(int iV : cfaces[iC]) V.push_back(cverts[iV]);
		Plane plane = Plane(V);

		//If the triangle degenerate skip it
		if(plane.is_degenerate) {
			if(partition_cuts[iP].size() > 1) 
				partition_cuts[iP][0] = partition_cuts[iP].back();
			partition_cuts[iP].pop_back();
			part_queue.push(iP);
			continue;
		}

		//Store the current part faces and cuts
		std::vector<int> cpart = partition_cuts[iP];
		cpart[0] = cpart.back();
		cpart.pop_back();
		std::vector<int> fpart = partition_faces[iP];

		//Indexes of the new partitions
		int iL = newPartition(iP);
		int iR = newPartition();
		   
		//Increase the cut list as to fit the next partition
		partition_cuts[iL].clear();
		partition_cuts.push_back({});

		//Sort the future cuts left and right of the current cut
		// and keep all the cuts remove for knowing where the obstacles are
		std::vector<int> cut_on_cut = {iC};
		plane.prepare_right_left_queries(&cverts);
		for(int iF : cpart) {
			int seen = false;
			for(int iV : cfaces[iF]) {
				if(plane.is_left(iV)) {
					partition_cuts[iL].push_back(iF);
					seen = true;
					break;
				}
			}
			for(int iV : cfaces[iF]) {
				if(plane.is_right(iV)) {
					partition_cuts[iR].push_back(iF);
					seen = true;
					break;
				}
			}
			if(!seen) cut_on_cut.push_back(iF);
		}

		//Sort the faces of the partition
		std::vector<int> inter_points;
		std::vector<std::vector<std::pair<int,int>>> inter;
		plane.prepare_right_left_queries(&wverts);
		plane.reset_on_plane();
		for(int iF : fpart) {
			bool isl = false, isr = false;
			std::vector<std::pair<int,int>> temp_tri;
			for(int iV : wfaces[iF]) {
				if(!plane.is_left(iV)) {
					if(!plane.is_right(iV)) {
						inter_points.push_back(iV);
						temp_tri.push_back({0, iV});
					} else {
						isr = true;
						temp_tri.push_back({1, iV});
					}
				} else {
					isl = true;
					temp_tri.push_back({-1, iV});
				}

			}
			if(isl) {
				if(isr) {
					std::sort(temp_tri.begin(), temp_tri.end());	
					temp_tri.push_back({iF, iF});
					inter.push_back(temp_tri);
				} else {
					partition_faces[iL].push_back(iF);
				}
			} else {
				if(isr) {
					fneighs.replace_neigh(iF, iL, iR);
					partition_faces[iR].push_back(iF);
				}
			}
		}

		//Modify the mesh to split the partition in two
		//Try to no recompute the intersections and minimize 
		//the number of new triangles

		plane.prepare_intersection_queries(&wverts);
		
		for(auto vec : inter) {
			std::pair<int,int> a = vec[0], b = vec[1], c = vec[2];
			int iF = vec[3].second, ia = a.second, ib = b.second, ic = c.second;
			int summ = a.first + b.first + c.first;
			std::vector<int> lFs, rFs;

			//Intersection plane and triangle
			int iv0 = plane.intersect(ia, ic);
			int if1, if2, iv1;

			switch(summ + 1) {
				case 1:
					//One point on the cutting plane thus only two new triangles
					wfaces[iF] = {ia, ib, iv0};
					partition_faces[iL].push_back(iF);

					if1 = add_elem(wfaces, {ic, ib, iv0});
					rFs.push_back(if1);
					partition_faces[iR].push_back(if1);
					break;

				case 2:
					//Intersection plane and triangle
					iv1 = plane.intersect(ia, ib);


					//Two points right of the cutting edge thus three new triangles
					wfaces[iF] = {ia, iv0, iv1};
					partition_faces[iL].push_back(iF);

					if1 = add_elem(wfaces, {ic, ib, iv0});
					rFs.push_back(if1);
					partition_faces[iR].push_back(if1);

					if2 = add_elem(wfaces, {ib, iv0, iv1});
					rFs.push_back(if2);
					partition_faces[iR].push_back(if2);
					break;
				case 0:
					//Intersection plane and triangle
					iv1 = plane.intersect(ib, ic);

					//Two points left of the cutting edge thus three new triangles
					wfaces[iF] = {ia, ib, iv0};
					partition_faces[iL].push_back(iF);

					if1 = add_elem(wfaces, {ib, iv0, iv1});
					lFs.push_back(if1);
					partition_faces[iL].push_back(if1);

					if2 = add_elem(wfaces, {ic, iv0, iv1});
					rFs.push_back(if2);
					partition_faces[iR].push_back(if2);
					break;
			}

			//Add the neighboors of the edges that were split by the cut
			std::pair<int, int> neighs = fneighs.get_neigh(iF);

			if(neighs.first >= 0) {
				int neigh = neighs.first + neighs.second - iP;
				if(neigh != neighs.first && neigh != neighs.second) 
					throw std::runtime_error("neigh is not in the neighs");
				for(int iF : rFs) {
					fneighs.set_neigh(iF, {iR, neigh});
					partition_faces[neigh].push_back(iF);
					partition_faces[iR].push_back(iF);
				}
				for(int iF : lFs) {
					fneighs.set_neigh(iF, {iL, neigh});
					partition_faces[neigh].push_back(iF);
					partition_faces[iL].push_back(iF);
				}
			} else {
				for(int iF : rFs) partition_faces[iR].push_back(iF);
				for(int iF : lFs) partition_faces[iL].push_back(iF);
			}
		}

		//Create the triangles that separate and link the two parts

		inter_points.insert(inter_points.end(), plane.on_plane.begin(), plane.on_plane.end());
		std::sort(inter_points.begin(), inter_points.end());
		inter_points.erase(std::unique(inter_points.begin(), inter_points.end()), 
					inter_points.end());

		//If the partition cut results in less than 3 corners on the cutting plane 
		//raise Error
		if(inter_points.size() < 3) 
			throw std::runtime_error("there are less than 3 corners in the cut this is not normal");

		//Create the triangles of the shared face (order may affect performance)
		std::vector<std::vector<int>> vs_cut = triangulate(inter_points, wverts, plane.N);

		for(auto f : vs_cut) {
			std::vector<Eigen::Matrix<double, 3, 1>> V = {wverts[f[0]], wverts[f[1]], wverts[f[2]]};
			Plane plane = Plane(V);
			if(plane.is_degenerate) continue; 
			int iF = add_elem(wfaces, f);
			partition_faces[iR].push_back(iF);
			partition_faces[iL].push_back(iF);
			fneighs.set_neigh(iF, {iL, iR});
		}

		//assertNoDegen(4);
		//When a partition has no more cuts it is time to decide if it is an obstacle or
		//if it is empty (we compare with the obstacle center of the last cut)
		if(!partition_cuts[iR].size()) {
			setPartitionBounds(iR);
			auto pcenter = (partition_bound[iR].first + partition_bound[iR].second) / 2;
			bool filled = false;
			for(int iCC : cut_on_cut) {
				if(cut_center_indexes[iCC] >= 0 && 
					(plane.N.dot(cut_centers[cut_center_indexes[iCC]]) - plane.T) * 
					(plane.N.dot(pcenter) - plane.T)> 0) {
					filled_partitions.push_back(iR);
					filled = true;
					break;
				}
			}
			if(!filled) part_queue.push(iR);
		} else part_queue.push(iR);

		if(!partition_cuts[iL].size()) {
			setPartitionBounds(iL);
			auto pcenter = (partition_bound[iL].first + partition_bound[iL].second) / 2;
			bool filled = false;
			for(int iCC : cut_on_cut) {
				if(cut_center_indexes[iCC] >= 0 && 
					(plane.N.dot(cut_centers[cut_center_indexes[iCC]]) - plane.T) * 
					(plane.N.dot(pcenter) - plane.T)> 0) {
					filled_partitions.push_back(iL);
					filled = true;
					break;
				}
			}
			if(!filled) part_queue.push(iL);
		} else part_queue.push(iL);
	}

	//Store resulting partition graph
	partition_graph = fneighs.get_graph_adjlist(partitionCount);
	assertGraphPossible();

	//Removing all the partitions that are actually obstacles
	sort(filled_partitions.begin(), filled_partitions.end(), 
		[](int a, int b){ return a > b;});
	for(int iP : filled_partitions) removePartition(iP);

	//Removing all partitions with null size
	for(int iP = partition_faces.size() - 1; iP >= 0; iP --) 
		if(partitionVolume(iP) < EPSE) removePartition(iP);

}


void Environment::removePartition(int iP) {
	//Removes a partition taking care of freeing up memory 
	//and updating partitionCount, and updating partition graph
	int miP = partition_faces.size() - 1;
	if(iP < miP) {
		partition_normal[iP] = partition_normal.back();
		partition_t[iP] = partition_t.back();
		partition_set[iP] = partition_set.back();
		partition_faces[iP] = partition_faces.back();
		partition_bound[iP] = partition_bound.back();
		partition_sampling[iP] = partition_sampling.back();
		partition_graph[iP] = partition_graph.back();
	}
	partition_normal.pop_back();
	partition_t.pop_back();
	partition_set.pop_back();
	partition_faces.pop_back();
	partition_bound.pop_back();
	partition_sampling.pop_back();
	partition_graph.pop_back();

	for(int iiP = 0; iiP < miP; iiP++) {
		unsigned int i = 0;
		while(i < partition_graph[iiP].size()) {
			if(partition_graph[iiP][i] == iP) {
				partition_graph[iiP][i] = partition_graph[iiP].back();
				partition_graph[iiP].pop_back();
			} else if(partition_graph[iiP][i] == miP) {
				partition_graph[iiP][i] = iP;
				i ++;
			} else i ++;
		}
	}
	partitionCount --;
}

int Environment::newPartition(int iP) {
	//Overwrites or returns new empty partition
	if(iP == -1) {
		iP = partition_faces.size();
		partition_normal.resize(iP + 1);
		partition_t.resize(iP + 1);
		partition_set.resize(iP + 1);
		partition_faces.resize(iP + 1);
		partition_bound.resize(iP + 1);
		partition_sampling.resize(iP + 1);
		partitionCount ++;
	}
	partition_set[iP] = false;
	partition_faces[iP].clear();
	return iP;
}

bool Environment::inPartition(Eigen::Matrix<double, 3, 1> p, int iP) {
	//checks if point is in partition iP
	if(((partition_bound[iP].first - p).array() > EPSE).any()) return false;
	if(((partition_bound[iP].second - p).array() < -EPSE).any()) return false;
	if(((partition_normal[iP] * p - partition_t[iP]).array() < -EPSE).any()) return false;
	return true;
};

void Environment::setPartitionBounds(int iP) {
	//Computes maximum and minimum x, y, z in the given partition
	if(!partition_faces[iP].size()) throw std::runtime_error("no faces to set");
	partition_bound[iP].first = wverts[wfaces[partition_faces[iP][0]][0]];
	partition_bound[iP].second = wverts[wfaces[partition_faces[iP][0]][0]];
	for(int iF : partition_faces[iP]) {
		for(int iV : wfaces[iF]) {
			partition_bound[iP].first = partition_bound[iP].first.cwiseMin(wverts[iV]);
			partition_bound[iP].second = partition_bound[iP].second.cwiseMax(wverts[iV]);
		}
	}
}

void Environment::setPartitionInclusionMatrix(int iP) {
	//Computes the delimiting planes which will be used to test inclusion
	setPartitionBounds(iP);
	Eigen::Matrix<double, 3, 1> center = (partition_bound[iP].first + partition_bound[iP].second) / 2;

	std::vector<Eigen::Matrix<double, 3, 1>> V;
	int nfaces = partition_faces[iP].size();
	int count_unique = 0;
	Eigen::Matrix<double, -1, 3> normals(nfaces,3);
	Eigen::Matrix<double, -1, 1> ts(nfaces,1);
	for(int i = 0; i < nfaces; i ++) {
		int iF = partition_faces[iP][i];
		Plane plane = Plane({wverts[wfaces[iF][0]], wverts[wfaces[iF][1]], wverts[wfaces[iF][2]]});
		if((plane.N.transpose() * center < plane.T)) {
			plane.N *= -1;
			plane.T *= -1;
		}
		normals.block(count_unique, 0, 1, 3) = plane.N.transpose();
		ts(count_unique, 0) = plane.T;

		count_unique ++;
		for(int j = 0; j < count_unique - 1; j++) {
			if(abs(ts(j,0) - ts(count_unique - 1,0)) < EPSE &&
				abs((normals.block(j, 0, 1, 3) -
					normals.block(count_unique - 1, 0, 1, 3)).squaredNorm()) < EPSE) {
				count_unique --;
				break;
			}
		}
	}

	partition_t[iP] = ts.block(0, 0, count_unique, 1);
	partition_normal[iP] = normals.block(0, 0, count_unique, 3);

	if(!inPartition(center, iP)) throw std::runtime_error("center not in its own partition"); 
}

int Environment::findPartition(Eigen::Matrix<double, 3, 1> p) {
	//Return partition containing p -2 if none
	std::vector<std::pair<double, int>> distance(partition_faces.size());
	for(int iP = 0; iP < partitionCount; iP ++) {
		distance[iP].first = (p - (partition_bound[iP].first + partition_bound[iP].second) / 2).squaredNorm();
		distance[iP].second = iP;
	}
	std::sort(distance.begin(), distance.end());
	for(auto dist : distance) if(inPartition(p, dist.second)) return dist.second;
	return -2;
}

void Environment::setPartitionMaxSize(double partitionMaxSize) {
	this->partitionMaxSize = partitionMaxSize;
}

void Environment::setVolumePoints(int pointsPerVolume) {
	this->pointsPerVolume = pointsPerVolume;
}

double Environment::partitionVolume(int iP) {
	//Returns an approximation of the partitions volume using
	// pointsPerVolume amount of points
	setPartitionBounds(iP);
	setPartitionInclusionMatrix(iP);
	partition_sampling[iP] = {0,0};
	int inside = 0;
	auto diagonal = partition_bound[iP].second - partition_bound[iP].first;
	for(int i = 0; i < pointsPerVolume; i ++) {
		Eigen::Matrix<double, 3, 1> p = partition_bound[iP].first + (((Eigen::Array<double, 3, 1>::Random() + 1) / 2) * 
			((partition_bound[iP].second - partition_bound[iP].first).array())).matrix();
		if(inPartition(p, iP)) inside ++;
	}
	double v = double(inside) / double(pointsPerVolume) * diagonal(0,0) * diagonal(1,0) * diagonal(2,0);
	partition_sampling[iP] = {pointsPerVolume, inside};
	return v;
}

Eigen::Matrix<double, 3, 1> Environment::samplePartition(int iP) {
	//Return random point in partition iP
	Eigen::Matrix<double, 3, 1> p;
	do {
		p = partition_bound[iP].first + (((Eigen::Array<double, 3, 1>::Random() + 1) / 2) * 
		((partition_bound[iP].second - partition_bound[iP].first).array())).matrix();
		//partition_sampling[iP].first ++;
	} while(!inPartition(p, iP));
	//partition_sampling[iP].second ++;
	return p;
}

void Environment::computePathLengths(Eigen::Matrix<double, 3, 1> goal) {
	//Computes distance to goal point from all partitions
	int goalP = findPartition(goal);
	if(goalP < 0) throw std::runtime_error("goal not in any partition");
	std::vector<Eigen::Matrix<double, 3, 1>> pcenters(partitionCount);
	for(int iP = 0; iP < partitionCount; iP++)
		pcenters[iP] = (partition_bound[iP].first + partition_bound[iP].second) / 2;
	pcenters[goalP] = goal;
	std::vector<std::vector<double>> dists(partitionCount);
	for(int iP = 0; iP < partitionCount; iP++) dists[iP].resize(partitionCount);
	for(int iP = 0; iP < partitionCount; iP++) {
		for(int jP = 0; jP < iP; jP++) {
			dists[iP][jP] = (pcenters[iP] - pcenters[jP]).norm();
			dists[jP][iP] = dists[iP][jP];
		}
	}

	//Djikstra
	std::vector<double> dist_to_goal(partitionCount, DBL_MAX);
	dist_to_goal[goalP] = 0;
	std::priority_queue<std::pair<double, int>> priority_queue;
	priority_queue.push({0, goalP});
	while(!priority_queue.empty()) {
		auto cur = priority_queue.top();
		priority_queue.pop();
		for(int next : partition_graph[cur.second]) {
			if(cur.first + dists[cur.second][next] < dist_to_goal[next]) {
				dist_to_goal[next] = cur.first + dists[cur.second][next];
				priority_queue.push({dist_to_goal[next], next});
			}
		}
	}
	path_lengths.resize(partitionCount);
	for(int iP = 0; iP < partitionCount; iP++) {
		for(int jP : partition_graph[iP]) {
			path_lengths[iP].push_back({dist_to_goal[jP] + dists[iP][jP] ,jP});
		}
	}
	this->goal = goal;
}
void Environment::printPartitions() {
	for(int iP = 0; iP < partitionCount; iP ++) {
		setPartitionBounds(iP);
		std::cout << iP << std::endl;
		std::cout << " min : " << partition_bound[iP].first.transpose() << std::endl <<
		" max : " << partition_bound[iP].second.transpose() << std::endl;
	}
}
void Environment::printPartition(int iP) {
	std::cout << " partition " << iP << std::endl;
	for(auto iF : partition_faces[iP]) {
		for(int iV : wfaces[iF]) std::cout << iV << " | ";
		std::cout << std::endl;
		for(int iV : wfaces[iF]) std::cout << wverts[iV].transpose() << " | ";
		std::cout << std::endl;
	}
}
void Environment::printPartitionSamplingData() {
	for(int iP = 0; iP < partitionCount; iP++) {
		std::cout << iP << " : " << 
		double(partition_sampling[iP].second) / double(partition_sampling[iP].first) << std::endl;
	}
}

void Environment::savePartitions() {

	{
		std::ofstream file(std::string("../visualize/vis/envv.csv").c_str());
		for(auto v : wverts) {
			file << v[0] << "," << v[1] << "," << v[2] << std::endl;
		}
		file.close();
	}
	{
		std::ofstream file(std::string("../visualize/vis/envf.csv").c_str());
		file << partition_faces.size() << std::endl;
		file.close();
	}
	{
		for(int iP = 0; iP < partitionCount; iP++) {
			std::ofstream file((std::string("../visualize/vis/envf") + std::to_string(iP) + ".csv").c_str());
			for(int iF : partition_faces[iP]) {
				auto f = wfaces[iF];
				file << f[0] << "," << f[1] << "," << f[2] << std::endl;
			}
			file.close();
		}
	}
}

void Environment::saveEnvironment() {
	Mesh m;
	for(auto obstacle : obstacles) m.merge(&obstacle);
	m.saveTo("../visualize/vis/obstacles");
	workspace.saveTo("../visualize/vis/workspace");

	std::vector<std::vector<int>> twfaces;
	std::vector<int> wfacetaken(wfaces.size());
	//std::cout << " there are " << partitionCount << " partitions " << std::endl;
	for(int iP = 0; iP < partitionCount; iP ++) {
		for(int iF : partition_faces[iP]) {
			//std::cout << iF << " ";
			if(!wfacetaken[iF]) {
				wfacetaken[iF] = 1;
				twfaces.push_back(wfaces[iF]);
			}
		}
		//std::cout << std::endl;
	}
	//std::cout << " here is the size of faces " << std::endl;
	//std::cout << twfaces.size() << std::endl;

	Mesh mm(wverts, twfaces);
	mm.saveAsLineTo("../visualize/vis/workspace");
}

void Environment::assertGraphPossible() {
	for(int iP = 0; iP < partitionCount; iP ++) 
		setPartitionBounds(iP);
	for(int iP = 0; iP < partitionCount; iP ++) 
	for(int jP : partition_graph[iP]) {
		auto xmin = partition_bound[iP].first;
		auto xmax = partition_bound[iP].second;
		auto ymin = partition_bound[jP].first;
		auto ymax = partition_bound[jP].second;
		bool p = 0;

		for(int i = 0; i < 3; i++) {
			int j = (i + 1) % 3;
			int k = (i + 2) % 3;
			if(xmin[i] - ymax[i] < EPSE &&
				xmin[j] - ymax[j] < -EPSE &&
				xmin[k] - ymax[k] < -EPSE) {
				p = 1;
			}
			if(ymin[i] - xmax[i] < EPSE &&
				ymin[j] - xmax[j] < -EPSE &&
				ymin[k] - xmax[k] < -EPSE) {
				p = 1;
			}
		}
		if(!p) {
			std::cout << " graph not possible " << std::endl;
			std::cout << iP << " " << jP << " cannot toucht" << std::endl;
			printPartitions();
			throw;
		}
	}
}

void Environment::assertNoDegen(int i) {
	if(i != -1) std::cout << " assert degen " << i << std::endl;
	//Check all triangles and throw if one has area 0
	for(int iP = 0; iP < partitionCount; iP ++) {
		for(int iF : partition_faces[iP]) {
			std::vector<Eigen::Matrix<double, 3, 1>> V = 
			{wverts[wfaces[iF][0]], wverts[wfaces[iF][1]], wverts[wfaces[iF][2]]};
			Plane plane = Plane(V);
			if(plane.is_degenerate) {
				std::cout << plane.N << std::endl;
				std::cout << " degenerate triangle : " << std::endl;
				std::cout << V[0] << std::endl;
				std::cout << V[1] << std::endl;
				std::cout << V[2] << std::endl;
				throw std::runtime_error("degenerate triangle found ");
			}
		}
	}
}
