#include "HierarchicalPlanner.h"
#include "Environment.h"

#include <ompl/base/SpaceInformation.h>
#include <ompl/base/spaces/SE3StateSpace.h>
#include <ompl/geometric/planners/rrt/RRTConnect.h>
#include <ompl/geometric/SimpleSetup.h>

#include <eigen3/Eigen/Dense>
#include <fstream>
#include <time.h>
#include <chrono>

#define EPSHP 1.0e-12

//TODO 
//work on the operator<

//This function defines in which order the jobs are taken
// this can drasticaly change time and length of path

#define PIECEFUNC(x) ((x < 0.1) ? x*9.0 : 0.89 + 0.1 * x)

bool operator<(const Job& j1, const Job& j2) { 
	return double(j1.points_explored) * j1.dist_left * j1.dist_left * PIECEFUNC(j2.success_rate) > 
		double(j2.points_explored) * j2.dist_left * j2.dist_left * PIECEFUNC(j1.success_rate); 
	//return double(j1.points_explored) * j1.dist_left * j1.dist_left * PIECEFUNC(j2.success_rate) > 
		//double(j2.points_explored) * j2.dist_left * j2.dist_left * PIECEFUNC(j1.success_rate); 
	//return double(j1.points_explored) * double(j1.points_explored) * j1.dist_left * j1.dist_left * PIECEFUNC(j2.success_rate) > 
		//double(j1.points_explored) * double(j1.points_explored) * j2.dist_left * j2.dist_left * PIECEFUNC(j1.success_rate); 
	//return double(j1.points_explored) * j1.dist_left * PIECEFUNC(j2.success_rate) > 
		//double(j1.points_explored) * j2.dist_left * PIECEFUNC(j1.success_rate); 
	//return double(j1.points_explored) * j1.dist_left * (0 + j2.success_rate) > 
		//double(j1.points_explored) * j2.dist_left * (0 + j1.success_rate); 
	//return j1.dist_left > j2.dist_left; 
	//return j1.dist_left * j2.success_rate > j2.dist_left * j1.success_rate; 
} 

namespace ompl
{
	HierarchicalPlanner::HierarchicalPlanner(const base::SpaceInformationPtr &si, int njoints, 
				double max_step_size, double new_partition_sample_rate,
				double invertibility_lambda, double polytope_rootstate_mindist,
				int job_time) : 
				base::Planner(si, "Incredible Hierarchical Planner") {
		this->njoints = njoints; 
		this->max_step_size = max_step_size; 
		this->new_partition_sample_rate = new_partition_sample_rate;
		this->invertibility_lambda = invertibility_lambda;
		this->polytope_rootstate_mindist = polytope_rootstate_mindist;
	}

	HierarchicalPlanner::~HierarchicalPlanner(void) {
	}

	void HierarchicalPlanner::clear(void) {
		Planner::clear();
	}

	void HierarchicalPlanner::getPlannerData(base::PlannerData &data) const {
	}

	void HierarchicalPlanner::setEnvironment(Mesh workspace, std::vector<Mesh> obstacles,
				double partitionMaxSize, int volumePoints) {
		environment = new Environment(workspace, obstacles);
		environment->setVolumePoints(volumePoints);
		environment->setPartitionMaxSize(partitionMaxSize);
		environment_set = true;
	}
	
	void HierarchicalPlanner::setPoseToWorld(pose_to_world_func_ptr pose_to_world, 
					pose_to_world_d_func_ptr pose_to_world_d) {
		this->pose_to_world = pose_to_world;
		this->pose_to_world_d = pose_to_world_d;
		pose_to_world_set = true;
	}

	void HierarchicalPlanner::setIsQValid(isQValid_func_ptr isQValid) {
		this->isQValid = isQValid;
		is_q_valid_set = true;
	}

	void HierarchicalPlanner::setMaxThreads(int max_threads) {
		this->max_threads = max_threads;
	}
	void HierarchicalPlanner::setVerbose(bool verbose) {
		this->verbose = verbose;
	}
	void HierarchicalPlanner::setVisualize(bool visualize) { 
		this->visualize = visualize;
	}
	void HierarchicalPlanner::setEndEffectorGoal(Eigen::Matrix<double, 3, 1> goal) {
		goalr = goal;
		end_effector_goal_set = true;
	}
	void HierarchicalPlanner::setupGoal() {
		const base::State *startst = pis_.nextStart();
		startq = state_to_vec(startst);
		startr = pose_to_world(startq);
		startF = environment->findPartition(startr);
		if(!isQReallyValid(startq))
			throw std::runtime_error("please give a valid start pose");

		if(!end_effector_goal_set) {
			const base::State *goalst = pis_.nextGoal();
			Eigen::Matrix<double, -1, 1> goalq = state_to_vec(goalst);
			goalr = pose_to_world(goalq);
		}
		goalF = environment->findPartition(goalr);
		if(goalF == -2) 
			throw std::runtime_error("please give a valid goal pose or end effector position");

		environment->computePathLengths(goalr);

		goal_setup = true;
	}

	void HierarchicalPlanner::setup(void) {
		//Setup of the planner with the information about the workspace given
		//call once after giving a new workspace

		if(!si_->isSetup()) si_->setup();
		Planner::setup();
		tools::SelfConfig sc(si_, getName());

		//Checking requierments
		if(!environment_set)
	 		throw std::runtime_error("set up called with out seting up environment");
		
		if(!pose_to_world_set)
	 		throw std::runtime_error("set up called with out state to world function and its derivative");

		if(!is_q_valid_set)
			throw std::runtime_error("set up called with out is q valid function (taking pose as eigen vector as input)");

		//Making bounds to eigen vectors to facilitate validity checking
		auto bounds = si_->getStateSpace()->as<ompl::base::RealVectorStateSpace>()->getBounds();
		qmax.resize(njoints, 1);
		qmin.resize(njoints, 1);
		for(int i = 0; i < njoints; i ++) {
			qmax[i] = bounds.high[i];
			qmin[i] = bounds.low[i];
		}

		environment->divideWorkspace();
		if(visualize) environment->saveEnvironment();

		partition_root_nodes_mutex = std::vector<std::mutex>(environment->partitionCount);
		partition_root_nodes.resize(environment->partitionCount);

		if(visualize) environment->savePartitions();

		if(verbose) {
			std::cout << "Partition Bounds  ------" << std::endl;
			environment->printPartitions();

			std::cout << "Partition Graph  ------" << std::endl;
			for(int i = 0; i < int(environment->partition_graph.size()); i ++) {
				std::cout << i << " : ";
				for(auto x : environment->partition_graph[i]) std::cout << x << " ";
				std::cout << std::endl;
			}

			std::cout << "Partition Volumes  ------" << std::endl;
			for(int iP = 0; iP < environment->partitionCount; iP++) {
				std::cout << iP << " : " << environment->partitionVolume(iP) << std::endl;
			}

			std::cout << "Partition Sampling Probability  ------" << std::endl;

			for(int iP = 0; iP < environment->partitionCount; iP++) {
				for(int i = 0; i < 100; i++) {
					Eigen::Matrix<double, 3, 1> p = environment->samplePartition(iP);
					if(environment->findPartition(p) != int(iP)) {
						std::cout << "Failed" << std::endl;
						std::cout << " sampeled partition " << iP << std::endl;
						std::cout << " got " << p.transpose() << std::endl;
						std::cout << " point is in partition " << environment->findPartition(p) << std::endl;
					}
				}
			}
			environment->printPartitionSamplingData();
		}
	}

	bool HierarchicalPlanner::isQReallyValid(Eigen::Matrix<double, -1, 1> q) {
		//Checking if q is a valid position with given function
		//and joint limits
		if(((q - qmax).array() > 0).any()) return false;
		if(((q - qmin).array() < 0).any()) return false;
		return isQValid(q);
	}
	Eigen::Matrix<double, Eigen::Dynamic, 1> HierarchicalPlanner::state_to_vec(const ompl::base::State *state) { 
		//Transform ompl state to eigen vector
		auto *rvstate = state->as<ompl::base::RealVectorStateSpace::StateType>();
		Eigen::Matrix<double, Eigen::Dynamic, 1> q(njoints);
		for(int i = 0; i < njoints; i ++) q(i, 0) = rvstate->values[i];
		return q;
	}
	ompl::base::State* HierarchicalPlanner::vec_to_state(Eigen::Matrix<double, Eigen::Dynamic, 1> vec) {
		//Transform eigen vector to ompl state
		auto* state = si_->allocState();
		auto* rvstate = state->as<ompl::base::RealVectorStateSpace::StateType>();
		for(int i = 0; i < njoints; i ++) rvstate->values[i] = vec(i, 0);
		return state;
	}
	Eigen::Matrix<double, -1, 1> HierarchicalPlanner::sampleQ() {
		//Sample random sate within bounds
		return qmin + (((Eigen::Array<double, -1, 1>::Random(njoints,1) + 1) / 2) * 
			((qmax - qmin).array())).matrix();
	}
	Eigen::Matrix<double, Eigen::Dynamic, 1> HierarchicalPlanner::QStep(
						Eigen::Matrix<double, Eigen::Dynamic, 1> q, 
						Eigen::Matrix<double, Eigen::Dynamic, 1> dq) {
		//Return the max step from q in the wanted direciton dq
		qstep_attemps ++;
		if(dq.squaredNorm() > max_step_size*max_step_size) {
			dq.normalize();
			dq *= max_step_size;
		}
		Eigen::Matrix<double, Eigen::Dynamic, 1> qnew = q + dq;
		if(isQReallyValid(qnew)) {
			states ++;
			return qnew;
		}
		return (Eigen::Matrix<double, Eigen::Dynamic, 1>) 0;
	}
	State_node* HierarchicalPlanner::extend(QState_node qtnear, Eigen::Matrix<double, 3, 1> rnew, Polytope_node *n) {
		//Try to add new position going in direction rnew from tnear
		//and if not possible explore null space
		Q_node *qnear = qtnear.first;
		State_node *tnear = qtnear.second;
		Eigen::Matrix<double, 3, -1> J = pose_to_world_d(qnear->q);
		Eigen::Matrix<double, -1, 3> Jesus;

		//Using pseudo inverse to compute next state direction
		if(abs((J*J.transpose()).determinant()) > EPSHP) {
			Jesus = J.transpose() * (J * J.transpose()).inverse();
		} else {
			Jesus = J.transpose() * (J * J.transpose() + invertibility_lambda 
					* Eigen::Matrix<double, 3, 3>::Identity()).inverse();
		}
		Eigen::Matrix<double, -1, 1> dq = Jesus * (rnew - tnear->r);
		Eigen::Matrix<double, -1, 1> qnew = QStep(qnear->q, dq);

		if(!qnew.rows()) {
			//Search null space
			//Using the J pseudo inverse to find element of the kernel
			Eigen::Matrix<double, -1, 1> qr = sampleQ();
			dq = (Eigen::Matrix<double, -1, -1>::Identity(njoints, njoints) -
				Jesus * J) * (qr - qnear->q);
			qnew = QStep(qnear->q, dq);
			if(qnew.rows()) {
				//Add the new null space movement to tree
				Q_node *qn_new = new Q_node{qnew, qnear};
				{
					std::lock_guard<std::mutex> lock(n->m);
					tnear->qs.push_back(qn_new);
				}
				null_space_states ++;
			}
			return nullptr;
		}
		//Add the new movement to tree
		Q_node *qn_new = new Q_node{qnew, qnear};
		State_node *tnew = new State_node{pose_to_world(qnew), {qn_new}};
		{
			std::lock_guard<std::mutex> lock(n->m);
			tnear->children.push_back(tnew);
		}
		position_states ++;
		return tnew;

	}
	QState_node HierarchicalPlanner::findNear(Polytope_node *n, Eigen::Matrix<double, 3, 1> r) {
		//Find nearest point in the current partition tree to r
		//Currently checking all in order and whole node locked
		std::lock_guard<std::mutex> lock(n->m);
		double minn = DBL_MAX;
		if(!n->ts.size()) throw std::runtime_error("no states in polytope node");
		State_node* mint = n->ts[0];
		for(auto t : n->ts) {
			double tmin = (t->r - r).squaredNorm();
			if(tmin < minn) {
				minn = tmin;
				mint = t;
			}
		}
		return {mint->qs[rand() % mint->qs.size()], mint};
	}
	State_node* HierarchicalPlanner::connectRegion(Job &job) {
		//Using random point connect to find path between two partitions
		Eigen::Matrix<double, 3, 1> rnew;
		State_node* tnew = nullptr;
		//long end_time = system.currentTimeMillis() + job_time;
		auto start = std::chrono::high_resolution_clock::now();
		int total = 0, successes = 0, tF;
		while(1) {
			auto cur = std::chrono::high_resolution_clock::now();
			if(solved || std::chrono::duration_cast<std::chrono::microseconds>
				(cur-start).count() > job_time) {
				tnew = nullptr;
				break;
			}
			for(int i = 0; i < 100; i++) {
				total ++;
				if(double(rand()) / double(RAND_MAX) < new_partition_sample_rate) {
					if(job.goal_partition < 0) rnew = goalr;
					else rnew = environment->samplePartition(job.goal_partition);
				} else rnew = environment->samplePartition(job.polytope_node->F);
				QState_node qtnear = findNear(job.polytope_node, rnew);
				tnew = extend(qtnear, rnew, job.polytope_node);
				if(!tnew) continue;
				successes ++;
				if(job.goal_partition < 0) {
					if((tnew->r - goalr).squaredNorm() < EPSHP) tF = -1;
					else tF = 0;
				} else tF = environment->findPartition(tnew->r);
				if(tF != job.goal_partition) {
					std::lock_guard<std::mutex> lock(job.polytope_node->m);
					job.polytope_node->ts.push_back(tnew);
				} else goto end;
			}
		}
		end:
		job.success_rate = double(successes)/double(total);
		job.nfails ++;
		job.points_explored += total;
		return tnew;
	}
	void HierarchicalPlanner::threadLoop() {
		//threads wait on a conditional variable until there are jobs
		//to be taken
		while(!solved) {
			{
				std::unique_lock<std::mutex> lock(thread_mutex);
				while (jobs_queue.empty() && !solved) thread_condvar.wait(lock);
			}

			if (solved) return;

			Job job = getJob();
			if(!job.polytope_node) continue;
			thread_condvar.notify_one();
			State_node *tnew = connectRegion(job);
			if(tnew) {
				if(job.goal_partition == -1) {
					if(solved) return;
					solved = true;
					thread_condvar.notify_all();
					{
						std::lock_guard<std::mutex> lock(job.polytope_node->m);
						Q_node *qgoal = tnew->qs[rand() % tnew->qs.size()];

						auto solution_path = 
							std::make_shared<ompl::geometric::PathGeometric>(si_);
						while(qgoal) {
							solution_path->append(vec_to_state(qgoal->q));
							qgoal = qgoal->parent;
						}
						pdef_->addSolutionPath(solution_path);
					}
				} else {
					Polytope_node *nnew = new Polytope_node;
					nnew->F = job.goal_partition;
					nnew->parent = job.polytope_node;
					nnew->ts.push_back(tnew);
					addAllJobsFrom(nnew, job);
				}
			} else addJob(job);
		}
	}

	Job HierarchicalPlanner::getJob(void) {
		//Thread safe job getter
		std::lock_guard<std::mutex> lock(jobs_queue_mutex);
		if(jobs_queue.empty()) return Job(0,0,0,0,0,0);
		Job job = jobs_queue.top();
		jobs_queue.pop();
		if(verbose) std::cout << " getting job " << job.polytope_node->F << " " << job.goal_partition << " try " << job.nfails << std::endl;
		return job;
	}

	void HierarchicalPlanner::addJob(Job &job) {
		//Thread safe job pusher
		std::lock_guard<std::mutex> lock(jobs_queue_mutex);
		jobs_queue.push(job);
	}

	void HierarchicalPlanner::addAllJobsFrom(Polytope_node *n, Job &job) {
		//Add all the jobs connecting polytope node n to all its neighbours
		// using the stats in job (success rate, points from origin)
		{
			std::lock_guard<std::mutex> lock(partition_root_nodes_mutex[n->F]);
			for(auto tn : partition_root_nodes[n->F]) {
				if((n->ts[0]->qs[0]->q - tn->ts[0]->qs[0]->q).squaredNorm()
					< njoints * polytope_rootstate_mindist) return;
			}
			partition_root_nodes[n->F].push_back(n);
		}
		std::lock_guard<std::mutex> lock(jobs_queue_mutex);
		if(n->F == goalF) jobs_queue.push({n, -1, job.success_rate, 1, job.nfails, job.points_explored});
		for(auto path : environment->path_lengths[n->F]) 
			jobs_queue.push({n, path.second, job.success_rate, path.first, job.nfails, job.points_explored});
	}

	base::PlannerStatus HierarchicalPlanner::solve(const base::PlannerTerminationCondition &ptc)
	{
		if(!goal_setup) throw std::runtime_error("solve called before setting up goal ( setupGoal() )");
		
		std::srand(std::time(NULL));

		if(verbose) printProblemInfo();

		resetCounters();

		jobs_queue = std::priority_queue<Job>();
		for(int i = 0; i < environment->partitionCount; i ++) 
			partition_root_nodes[i].clear();

		//Initialising trees
		Q_node *startqn = new Q_node{startq};
		State_node *troot = new State_node{startr, {startqn}};
		Polytope_node *nroot = new Polytope_node{startF, {troot}};

		Job job = {0, 0, 1, 0, 0, 1};
		addAllJobsFrom(nroot, job);

		solved = false;

		std::vector<std::thread> threads;
		for(int i = 0; i < max_threads; i++) threads.push_back(
			std::thread(&ompl::HierarchicalPlanner::threadLoop, this));
		for(int i = 0; i < max_threads; i++) threads[i].join();

		if(verbose) printSolverInfo();
		return base::PlannerStatus::EXACT_SOLUTION;
	}

	void HierarchicalPlanner::resetCounters() {
		qstep_attemps = states = null_space_states = position_states = 0;
	}

	void HierarchicalPlanner::printProblemInfo() {
		//Space information
		std::cout << "Searching in real vector space with :" << std::endl;
		std::cout << "maximum joint values : " << qmax.transpose() << std::endl;
		std::cout << "minimum joint values : " << qmin.transpose() << std::endl;
		std::cout << "maximum step size : " << max_step_size << std::endl;
		std::cout << "new partition sample rate : " << new_partition_sample_rate << std::endl;
		std::cout << std::endl;

		//Printing problem definition
		std::cout << "Start position : " << std::fixed << std::setw(5) << 
				std::setprecision( 3 ) << startr.transpose() << std::endl;
		std::cout << "Start partition : " << startF << std::endl;
		std::cout << std::endl;

		std::cout << "Goal position : " << std::fixed << std::setw(5) << 
				std::setprecision( 3 ) << goalr.transpose() << std::endl;
		std::cout << "Goal partition : " << goalF << std::endl;
		std::cout << std::endl;

	}

	void HierarchicalPlanner::printSolverInfo() {
		//Data collected during planning
		std::cout << "During solving : " << std::endl;
		std::cout << "states explored : " << states << std::endl;
		std::cout << "new states attempted : " << qstep_attemps << std::endl;
		std::cout << "null space states : " << null_space_states << std::endl;
		std::cout << "positions in 3D space : " << position_states << std::endl;
		std::cout << std::endl;
	}
}

