#include "Plane.h"
#include <iostream>

#define EPSPL 1.0e-10

Plane::Plane(std::vector<Eigen::Matrix<double, 3, 1>> V) {
	N = (V[0] - V[1]).cross(V[0] - V[2]);
	is_degenerate = N.squaredNorm() < EPSPL;
	if(!is_degenerate) N.normalize();
	else N = N.Zero();
	T = N.dot(V[0]);
}


void Plane::reset_on_plane() {
        on_plane.clear();
}

void Plane::prepare_right_left_queries(std::vector<Eigen::Matrix<double, 3, 1>>* L) {
        /*Prepare the plane to querie if points in L 
        are to the left or right of the plane by indice*/
        this->L = L;
	left.clear();
	left.resize(L->size(), -1);
	right.clear();
	right.resize(L->size(), -1);
}

void Plane::calc(int i) {
	left[i] = (N.dot((*L)[i]) - T < - 100*EPSPL);
	right[i] = (N.dot((*L)[i]) - T > 100*EPSPL);
	if(!right[i] && !left[i]) on_plane.push_back(i);
}

bool Plane::is_left(int i) {
        if(left[i] < 0) calc(i);
        return bool(left[i]);
}

bool Plane::is_right(int i) {
        if(right[i] < 0) calc(i);
        return bool(right[i]);
}

void Plane::prepare_intersection_queries(std::vector<Eigen::Matrix<double, 3, 1>>* L) {
        /*Prepare the plane to querie the intersection of two points
        in list L and the plane by indice*/
	this->L = L;
	inters.clear();
        inters.resize(L->size());
	on_plane_inter.clear();
	on_plane_inter.resize(L->size());
}

int Plane::intersect(int ia, int ib) {
        /*returns the intersection between the line generate by ia and ib 
        and the plane*/
	if(ia == ib) throw std::runtime_error("asking for intersection of plane and degenerate line");
        if(on_plane_inter[ia]) return ia;
        if(on_plane_inter[ib]) return ib;

	if(ia > ib) {
		int t = ib;
		ib = ia;
		ia = t;
	}

	for(std::pair<int,int> mem : inters[ia]) 
		if(mem.first == ib) return mem.second;

        Eigen::Matrix<double, 3, 1> va = (*L)[ia], vb = (*L)[ib];
	if(abs(N.dot(va - vb)) < EPSPL) 
		throw std::runtime_error("intersecting line and plane are parallel");

        double lam = (T - N.dot(vb)) / N.dot(va - vb);
	if(lam < 0. || lam > 1.) 
		throw std::runtime_error("both points are on the same side of the plane");

	int ix = L->size();
	L->push_back(lam*va + (1.0-lam) * vb);

	inters[ia].push_back({ib, ix});
	inters.push_back({});

	on_plane_inter.push_back(true);

	bool is_on = false;
	for(auto v : on_plane) {
		if(((*L)[v] - (*L)[ix]).squaredNorm() < EPSPL) {
			is_on = true;
			break;
		}
	}
	if(!is_on) on_plane.push_back(ix);

        return ix;
}
