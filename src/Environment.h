#ifndef ENVIRONMENT_H
#define ENVIRONMENT_H

#include <ompl/geometric/PathGeometric.h>
#include <ompl/base/Planner.h>
#include <ompl/util/RandomNumbers.h>
#include <ompl/tools/config/SelfConfig.h>

#include <fcl/narrowphase/collision.h>
#include <fcl/geometry/bvh/BVH_model.h>
#include <fcl/math/bv/OBBRSS.h>

#include "Mesh.h"

class Environment {
	/*
	This class stores the workspace and obstacles convex polytopes to 
	perform a partitioning of the workspace and obtain a graph of empty partitions.

	Then computes the minimum distance of each partition to a given goal point

	And implements partition sampling, find partition of a point, for the planners use.
	*/
public:
	int partitionCount = 0; 
	int pointsPerVolume = 10;
	int partitionMaxSize = 100;
	std::vector<Mesh> obstacles;
	Mesh workspace;

	Eigen::Matrix<double, 3, 1> goal = Eigen::Matrix<double, 3, 1>::Zero();

	std::vector<Eigen::Matrix<double, -1, 3>> partition_normal;
	std::vector<Eigen::Matrix<double, -1, 1>> partition_t;
	std::vector<std::pair<Eigen::Matrix<double, 3, 1>, Eigen::Matrix<double, 3, 1>>> partition_bound;
	std::vector<bool> partition_set;
	std::vector<Eigen::Matrix<double, 3, 1>> wverts;
	std::vector<std::vector<int>> wfaces, partition_faces, partition_graph;
	std::vector<std::pair<int, int>> partition_sampling;
	std::vector<std::vector<std::pair<double, int>>> path_lengths; 

	Environment(Mesh &workspace, std::vector<Mesh> &obstacles); 
	~Environment();

	void divideWorkspace();
	bool inPartition(Eigen::Matrix<double, 3, 1> p, int iP);
	int newPartition(int iP = -1);
	void setPartitionBounds(int iP);
	void setPartitionInclusionMatrix(int iP);
	void setVolumePoints(int volumePoints);
	void setPartitionMaxSize(double partitionMaxSize);
	int findPartition(Eigen::Matrix<double, 3, 1> p);
	Eigen::Matrix<double, 3, 1> samplePartition(int iP);
	void printPartitions();
	void printPartition(int iP);
	void printPartitionSamplingData();
	void printProblemInfo();
	void removePartition(int iP);
	void resetCounters();
	void assertNoDegen(int i = -1);
	void assertGraphPossible();
	double partitionVolume(int iP);
	void savePartitions();
	void computePathLengths(Eigen::Matrix<double, 3, 1> goal);
	void saveEnvironment();
};

#endif
