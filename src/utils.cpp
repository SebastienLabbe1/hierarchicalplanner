#include "utils.h"
#define EPSU 1.0e-12

std::vector<std::string> load_name_file(std::string filename) {
	std::ifstream file (filename.c_str(), std::ios::in);
	std::string buf = "hello";
	std::vector<std::string> vec;
	while(1) {
		buf = "";
		file >> buf;
		if(buf == "") break;
		vec.push_back(buf);
	}
	return vec;
}

Mesh* stl_to_mesh(std::string filename) {
	std::ifstream file (filename.c_str(), std::ios::in);

	if(!file) return nullptr;
	//std::streambuf *cinbuf = std::cin.rdbuf();
	std::cin.rdbuf(file.rdbuf());

	std::string buf;

	std::cin >> buf;
	if(buf != "solid") return nullptr;

	std::vector<Eigen::Matrix<double, 3, 1>> verts;
	std::vector<std::vector<int>> faces;
	Eigen::Matrix<double, 3, 1> v[3];
	Eigen::Matrix<double, 3, 1> n;
	std::vector<int> face;

	while(buf != "facet") std::cin >> buf;
	while(buf == "facet") {
		std::cin >> buf;
		std::cin >> n[0] >> n[1] >> n[2];
		std::cin >> buf >> buf >> buf;
		std::cin >> v[0][0] >> v[0][1] >> v[0][2];
		std::cin >> buf;
		std::cin >> v[1][0] >> v[1][1] >> v[1][2];
		std::cin >> buf;
		std::cin >> v[2][0] >> v[2][1] >> v[2][2];
		std::cin >> buf >> buf >> buf;

		face = {-1, -1, -1};
		for(int i = 0; i < 3; i++) {
			for(unsigned int j = 0; j < verts.size(); j ++) {
				if((verts[j] - v[i]).squaredNorm() < EPSU ) {
					face[i] = j;
					break;
				}
			}
			if(face[i] == -1) {
				face[i] = verts.size();
				verts.push_back(v[i]);
			}
		}
		faces.push_back(face);
	}

	Mesh * s = new Mesh(verts, faces);
	//std::cout << " read " << filename << std::endl;
	//s->print();
	
	return s;
}

inline double area(std::pair<double, double> x, std::pair<double, double> y, std::pair<double, double> z) {
	return (y.first - x.first) * (z.second - x.second) - (y.second - x.second) * (z.first - x.first); 
}

std::vector<std::vector<int>> triangulate(std::vector<int> iV, std::vector<Eigen::Matrix<double, 3, 1>> V, Eigen::Matrix<double, 3, 1> N) {
	if(iV.size() < 3) return {};
	int iM;
	if(abs(N[0]) > abs(N[1])) {
		if(abs(N[0]) > abs(N[2])) {
			iM = 0;
		} else {
			iM = 2;
		}
	} else {
		if(abs(N[1]) > abs(N[2])) {
			iM = 1;
		} else {
			iM = 2;
		}
	}
	std::vector<std::pair<int,int>> ring(iV.size());
	for(unsigned int i = 3; i < ring.size(); i ++) ring[i] = {-1, -1};
	int i1, i2, i3;
	do {
		i1 = std::rand() % iV.size();
		do {
			i2 = std::rand() % iV.size();
		} while(i1 == i2);
		do {
			i3 = std::rand() % iV.size();
		} while(i1 == i3 || i2 == i3);
		//std::cout << "stuckk" <<std::endl;
	} while((V[iV[i2]] - V[iV[i1]]).cross(V[iV[i3]] - V[iV[i1]])[iM] == 0);
	std::vector<std::vector<int>> triangs = {{i1,i2,i3}};
	if((V[iV[i2]] - V[iV[i1]]).cross(V[iV[i3]] - V[iV[i1]])[iM] < 0){
		ring[i1] = {i2, i3};
		ring[i2] = {i3, i1};
		ring[i3] = {i1, i2};
	} else {
		ring[i1] = {i3, i2};
		ring[i2] = {i1, i3};
		ring[i3] = {i2, i1};
	}
	int pring = i1;
	for(int i = 0; i < int(iV.size()); i ++) {
		if(i == i1 || i == i2 || i == i3) continue;
		int ri = pring;
		int ca = -1, cb;
		do {
			int next = ring[ri].first;
			if((V[iV[next]] - V[iV[ri]]).cross(V[iV[i]] - V[iV[ri]])[iM] > EPSU) {
				ca = ri;
				cb = next;
				break;
			} 
			ri = next;
		} while(pring != ri);
		if(ca < 0) continue;
		ring[cb].second = i;
		ring[ca].first = i;
		ring[i] = {cb, ca};
		triangs.push_back({ca, i, cb});
	}
	std::vector<std::vector<int>> ftriangs; 
	for(auto tri : triangs) {
		ftriangs.push_back({iV[tri[0]], iV[tri[1]], iV[tri[2]]});
	}
	return ftriangs;
}
