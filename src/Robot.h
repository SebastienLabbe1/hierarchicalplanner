#ifndef ROBOT_H
#define ROBOT_H

#include <vector>
#include <string>
#include <fstream>
#include <iostream>
#include <fcl/narrowphase/collision.h>
#include <fcl/geometry/bvh/BVH_model.h>
#include <fcl/math/bv/OBBRSS.h>

class Mesh;

class MeshRobot {
	public:
		std::vector<std::vector<double>> dh_table;
		std::vector<Mesh> menclosures;
		std::vector<double> joint_types;
		std::vector<int> joint_list;
		Eigen::Matrix<double, Eigen::Dynamic, 1> pose;
		std::vector<std::shared_ptr<fcl::BVHModel<fcl::OBBRSS<double>>>> collision_geometries;
		std::vector<fcl::CollisionObject<double>> collision_objects;
		std::vector<int> link_empty;
		fcl::CollisionRequest<double> coll_req;
		std::vector<Eigen::Matrix<double, 4, 4>> _to_world_trafos, _to_link_trafos;
		Eigen::Matrix<double, 4, 4> _base_to_world_trafo;
		int num_links;
		MeshRobot();
		MeshRobot(std::vector<std::vector<double>> dh_table, std::vector<double> joint_types,
			std::vector<double> pose, std::vector<Mesh> menclosures,
			Eigen::Matrix<double, 3, 1> position,
			Eigen::Matrix<double, 3, 3> orientation);
		~MeshRobot();

		void update_pose(std::vector<double> pose);
		void update_pose(Eigen::Matrix<double, Eigen::Dynamic, 1> pose);
		void _init_mesh_geometry();
		void _calc_trafos_to_world();
		bool check_collision(std::vector<fcl::CollisionObject<double>> &obstacle);
		bool check_collision(fcl::CollisionObject<double>* obstacle);
		bool check_self_collision();
		void saveTo(std::string filename, Eigen::Matrix<double, Eigen::Dynamic, 1> pose);
		Mesh* getMesh(Eigen::Matrix<double, Eigen::Dynamic, 1> pose);
};

#endif
