#include "Mesh.h"
#include <iostream>
#include <fstream>
#include <queue>

Mesh::Mesh(std::vector<Eigen::Matrix<double, 3, 1>> verts, std::vector<std::vector<int>> faces, 
		Eigen::Matrix<double, 3, 1> shift, Eigen::Matrix<double, 3, 3> rotation) {
	this->verts = verts;
	this->faces = faces;
	this->shift = shift;
	this->rotation = rotation;
}

Mesh::~Mesh() {
}

void Mesh::setShift(Eigen::Matrix<double, 3, 1> shift) {
	this->shift = shift;
}

void Mesh::setSize(double size) {
	rotation = size * rotation;
}

void Mesh::setScale(double sx, double sy, double sz) {
	Eigen::Matrix<double, 3, 3> t = Eigen::Matrix<double, 3, 3>::Zero();
	t(0, 0) = sx;
	t(1, 1) = sy;
	t(2, 2) = sz;
	rotation *= t;
}

void Mesh::setTransformation(Eigen::Matrix<double, 4, 4> trafo) {
	shift = trafo.block(0, 3, 3, 1);
	rotation = trafo.block(0, 0, 3, 3);
}

void Mesh::applyStoredTransformation() {
	for(unsigned int i = 0; i < verts.size(); i ++) verts[i] = rotation * verts[i] + shift;
	rotation = rotation.Identity();
	shift = shift.Zero();
}

void Mesh::merge(Mesh* mesh) {
	applyStoredTransformation();
	mesh->applyStoredTransformation();
	int n = int(verts.size());
	verts.insert(verts.end(), mesh->verts.begin(), mesh->verts.end());
	std::vector<std::vector<int>> tfaces = mesh->faces;
	for(int i = 0; i < int(tfaces.size()); i ++) 
		for(int j = 0; j < 3; j ++) 
			tfaces[i][j] += n;
	faces.insert(faces.end(), tfaces.begin(), tfaces.end());
}

void Mesh::print() {
	std::cout << "verts " << std::endl;
	for(auto v : verts) std::cout << v.transpose() << std::endl;
	std::cout << "faces " << std::endl;
	for(auto f : faces) std::cout << f[0] << " " << f[1] << " " << f[2] << std::endl;
}

void Mesh::saveTo(std::string filename) {
	std::ofstream vfile((filename + "v.csv").c_str());
	for(auto v : verts) vfile << v[0] << "," << v[1] << "," << v[2] << std::endl;
	vfile.close();
	std::ofstream ffile((filename + "f.csv").c_str());
	for(auto f : faces) ffile << f[0] << "," << f[1] << "," << f[2] << std::endl;
	ffile.close();
}
void Mesh::saveAsLineTo(std::string filename) {
	std::ofstream file((filename + "l.csv").c_str());
	std::vector<int> ivs;
	std::queue<int> q;
	for(int iF = 1; iF < int(faces.size()); iF ++) q.push(iF);
	if(faces.size()) { 
		ivs = faces[0];
		int fails = 0;
		while(!q.empty()) {
			int iF = q.front();
			auto face = faces[iF];
			q.pop();
			int found = 0;
			if(fails > 50) {
				fails = 0;
				ivs.insert(ivs.end(), {face[0], face[1], face[2]});
				continue;
			}
			for(int i = 0; i < int(ivs.size()); i ++) {
				if(ivs[i] == face[0]) {
					ivs.insert(ivs.begin()+i, {face[0], face[1], face[2]});
					found = 1;
				} else if (ivs[i] == face[1]) {
					ivs.insert(ivs.begin()+i, {face[1], face[2], face[0]});
					found = 1;
				} else if (ivs[i] == face[2]) {
					ivs.insert(ivs.begin()+i, {face[2], face[0], face[1]});
					found = 1;
				}
				if(found) break;
			}
			if(!found) {
				q.push(iF);
				fails ++;
			} else {
				fails = 0;
			}
		}
	}
	for(int iV : ivs) file << verts[iV][0] << "," << verts[iV][1] << "," << verts[iV][2] << std::endl;
	file.close();
}
