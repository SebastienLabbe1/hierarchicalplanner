#!/bin/bash
echo Running problem $1
rm ../visualize/vis/* -f
make -j 8
./prog $1
python3 -W ignore ../visualize/pathLengths.py $1
python3 -W ignore ../visualize/seeSolutions.py
#Uncomment to create latex tables with the results
#python3 -W ignore ../visualize/createTable.py $1 

