#ifndef FACENEIGH_H
#define FACENEIGH_H
#include <eigen3/Eigen/Eigen>

class FaceNeigh { 
	/*
	Class to maintain and modify the neighbour relations 
	of the partitions when cuting the workstation
	*/
	public:
		int offset;
		std::vector<std::pair<int, int>> neighboors;
		std::vector<bool> lfaces;

    		FaceNeigh(int offset);
		std::pair<int, int> get_neigh(int iF);
		void set_neigh(int iF, std::pair<int, int> neighs);
		void replace_neigh(int iF, int old, int next);
		std::vector<std::vector<int>> get_graph_adjlist(int N);
};

#endif
