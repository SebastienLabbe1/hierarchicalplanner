#include "FaceNeigh.h"
#include <iostream>

FaceNeigh::FaceNeigh(int offset) {
	/*Offset is the length current list of current faces so that
	translation from full list index to object list index 
	can be done internaly*/
	this->offset = offset;
	neighboors.clear();
	lfaces.clear();
}

std::pair<int, int> FaceNeigh::get_neigh(int iF) {
     	//Returns the neighboors of the face of index iF
        iF -= offset;
	if(int(neighboors.size()) > iF && iF >= 0) 
		return neighboors[iF];
        return {-1,-1};
}

void FaceNeigh::set_neigh(int iF, std::pair<int, int> neighs) {
        //Sets the neigh of the face of index iF
        iF -= offset;
        if(iF < 0) return;
	while(iF >= int(neighboors.size())) {
		neighboors.push_back({-1,-1});
	}
        neighboors[iF] = neighs;
}

void FaceNeigh::replace_neigh(int iF, int old, int next) {
        //If old is in the neighboors of the face iF then 
	//replace it with new
        iF -= offset;
	if(int(neighboors.size()) > iF && iF >= 0 
			&& neighboors[iF].first >= 0) {
		if(neighboors[iF].first == old) 
			neighboors[iF].first = next;
		if(neighboors[iF].second == old) 
			neighboors[iF].second = next;
	}
}
		
std::vector<std::vector<int>> FaceNeigh::get_graph_adjlist(int N) {
	//Returns a graph of the partition connections as an adjancecy list
	std::vector<std::vector<int>> list(N);
	for(auto neigh : neighboors) {
		int a = neigh.first;
		int b = neigh.second;
		if(a == -1) continue;
		if(list[a].size() > list[b].size()) {
			int t = a;
			a = b;
			b = t;
		}
		bool in = false;
		for(int i : list[a]) {
			if(i == b) {
				in = true;
				break;
			}
		}
		if(in) continue;
		list[a].push_back(b);
		list[b].push_back(a);
	}
	return list;
}
