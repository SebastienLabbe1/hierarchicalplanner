#include <ompl/base/SpaceInformation.h>
#include <ompl/base/DiscreteMotionValidator.h>
#include <ompl/base/spaces/SE3StateSpace.h>
#include <ompl/geometric/planners/rrt/RRTConnect.h>
#include <ompl/geometric/planners/rrt/RRTstar.h>
#include <ompl/geometric/SimpleSetup.h>

#include <ompl/config.h>
#include <iostream>
#include <chrono>

#include "HierarchicalPlanner.h"
#include "utils.h"
#include "Robot.h"
#include "Mesh.h"
#include "test.h"
#include "fwdkin.h"


//folder macros
#define DATA "../data/"
#define ROBOT "robots/"
#define ENV "environments/"
#define PROB "problems/"
#define STLFILE "stlfiles/"

int jointCount = 0;
std::vector<MeshRobot*> robots;
std::vector<std::mutex> robot_mutexes;
std::atomic<int> robot_index = 0;
std::mutex robot_index_mutex;
std::vector<double> qstart, qgoal;
Eigen::Matrix<double, -1, 1> qmax, qmin;
fcl::CollisionObject<double>* env;
Mesh workspace;
std::vector<Mesh> obstacles;

int volumePoints = 100;
double partitionMaxSize = 100;
int nrobots = 8;
double invertibility_lambda = 0.01;
double polytope_rootstate_mindist = 0.1;
double time_limit = 10.;
double checking_res = .01;
double rrt_checking_res = .01;
double rrt_motion_range = 1.;
double new_sample_rate = .9;
int job_time = 1000000;
int intermediate_poses = 3;
int nthreads = 10;
bool visualize = false;
bool verbose = false;
int solveCount = 1;
double optimalSolutionTime = 5;

std::shared_ptr<ompl::base::SpaceInformation> si;
std::shared_ptr<ompl::base::ProblemDefinition> pdef;
std::shared_ptr<ompl::base::RealVectorBounds> bounds;
std::shared_ptr<ompl::base::RealVectorStateSpace> space;
std::shared_ptr<ompl::base::DiscreteMotionValidator> motion_validator;

Eigen::Matrix<double, 3, 1> goal;
bool using_goal = false;

void loadMetaVariables(std::string filename) {
	std::string name = "";
	name += DATA + filename + ".txt";
	std::ifstream file(name, std::ios::in);
	std::string var_name;
	double var;
	std::cout << "Meta Variables -----" << std::endl;
	while(file.good()) {
		var_name = "";
		file >> var_name;
		if(var_name == "") break;
		if(var_name[0] == '/' && var_name[1] == '/') continue;
		file >> var;
		std::cout << var_name << " " << var << std::endl;
		if(var_name == "volumePoints") volumePoints = var;
		else if(var_name == "partitionMaxSize") partitionMaxSize = var;
		else if(var_name == "visualize") visualize = var;
		else if(var_name == "verbose") verbose = var;
		else if(var_name == "nrobots") nrobots = var;
		else if(var_name == "invertibility_lambda") invertibility_lambda = var;
		else if(var_name == "polytope_rootstate_mindist") polytope_rootstate_mindist = var;
		else if(var_name == "time_limit") time_limit = var;
		else if(var_name == "checking_res") checking_res = var;
		else if(var_name == "rrt_checking_res") rrt_checking_res = var;
		else if(var_name == "rrt_motion_range") rrt_motion_range = var;
		else if(var_name == "new_sample_rate") new_sample_rate = var;
		else if(var_name == "job_time") job_time = var;
		else if(var_name == "intermediate_poses") intermediate_poses = var;
		else if(var_name == "nthreads") nthreads = var;
		else if(var_name == "solveCount") solveCount = var;
		else if(var_name == "optimalSolutionTime") optimalSolutionTime = var;
		else throw std::runtime_error((var_name + " is not an existing global variable ").c_str());
	}
}

Eigen::Vector3d pose_to_world(Eigen::Matrix<double, Eigen::Dynamic, 1> pose) {
	return fwdkin_hom_mod(pose, robots[0]->dh_table, robots[0]->joint_types, robots[0]->_base_to_world_trafo);
}

Eigen::Matrix<double, 3, Eigen::Dynamic> pose_to_world_d(Eigen::Matrix<double, Eigen::Dynamic, 1> pose) {
	return fwdkin_hom_mod_d(pose, robots[0]->dh_table, robots[0]->joint_types, robots[0]->_base_to_world_trafo);
}

int getFreeRobot() {
	std::lock_guard<std::mutex> lock(robot_index_mutex);
	robot_index = (robot_index + 1) % nrobots;
	return robot_index;
}

bool isQValid(Eigen::Matrix<double, Eigen::Dynamic, 1> q) {
	int irobot = getFreeRobot();
	std::lock_guard<std::mutex> lock(robot_mutexes[irobot]);
	robots[irobot]->update_pose(q);
	if(robots[irobot]->check_self_collision()) return false;
	if(robots[irobot]->check_collision(env)) return false;
	return true;
}

bool isStateValid(const ompl::base::State *state) {
	auto *rvstate = state->as<ompl::base::RealVectorStateSpace::StateType>();
	Eigen::Matrix<double, -1, 1> q(jointCount, 1);
	for(int i = 0; i < jointCount; i ++) q(i, 0) = rvstate->values[i];
	return isQValid(q);
}

int loadRobot(std::string name, Eigen::Matrix<double, 4, 4> trafo, double scaling) {
	//Load the robot csv data
	std::string folder = std::string(DATA) + ROBOT + name + "/";
	std::vector<std::vector<double>> dh_tab_data = load_csv_file<double>(folder + "DHext.csv");
	jointCount = dh_tab_data.size();
	for(int i = 0; i < jointCount; i ++) {
		dh_tab_data[i][0] *= scaling;
		dh_tab_data[i][3] *= scaling;
		dh_tab_data[i][5] *= scaling;
	}
	std::vector<double> qtemp;
	qmax.resize(jointCount,1);
	qtemp = flatten(load_csv_file<double>(folder + "qmax.csv"));
	for(int i = 0; i < jointCount; i ++) qmax(i, 0) = qtemp[i];

	qmin.resize(jointCount,1);
	qtemp = flatten(load_csv_file<double>(folder + "qmin.csv"));
	for(int i = 0; i < jointCount; i ++) qmin(i, 0) = qtemp[i];

	Eigen::Matrix<double, 3, 1> position = trafo.block(0, 3, 3, 1);
	Eigen::Matrix<double, 3, 3> orient = trafo.block(0, 0, 3, 3);

	std::vector<double> jt_data = flatten(load_csv_file<double>(folder + "jt.csv"));
	std::vector<Mesh> meshs;
	auto names = load_scsv_file<std::string>(folder + "linkfilenames.txt");
	std::string stlfolder = std::string(DATA) + STLFILE;

	for(auto lnames : names) {
		Mesh* link = new Mesh();
		for(auto name : lnames) {
			Mesh* tlink = stl_to_mesh(stlfolder + name + ".stl");
			link->merge(tlink);
		}
		link->setSize(scaling);
		link->applyStoredTransformation();
		meshs.push_back(*link);
	} 
	robot_mutexes = std::vector<std::mutex>(nrobots);
	robots.resize(nrobots);
	for(int i = 0; i < nrobots; i ++)
		robots[i] = new MeshRobot(dh_tab_data, jt_data, qtemp, meshs, position, orient);
	return 0;
}

int loadEnv(std::string name, Eigen::Matrix<double, 4, 4> trafo) {
	Eigen::Matrix<double, 4, 4> transfo;
	std::string buf1, buf2;
	std::string envfolder = std::string(DATA) + ENV;
	std::string stlfolder = std::string(DATA) + STLFILE;
	std::ifstream file ((envfolder + name + ".txt").c_str(), std::ios::in);
	Mesh* tempm;
	while(1) {
		buf1 = "";
		file >> buf1;
		if(buf1 != "workspace" && buf1 != "obstacle") break;
		file >> buf2;
		tempm = stl_to_mesh(stlfolder + buf2);
		if(!tempm) return 1;
		for(int i = 0; i < 4; i++) 
			for(int j = 0; j < 4; j ++)
				file >> transfo(i,j);
		tempm->setTransformation(trafo * transfo);
		tempm->applyStoredTransformation();
		if(buf1 == "workspace") workspace = *tempm;
		else obstacles.push_back(*tempm);
	}

	obstacles.push_back(workspace);
	env = meshs_to_coll<double>(obstacles);
	obstacles.pop_back();
	return 0;
}

int loadProblem(std::string name) {
	std::string folder = std::string(DATA) + PROB + name + "/";

	auto names = load_name_file(folder + "description.txt");
	if(names.size() != 3) throw std::runtime_error("problem description incomplete");

	//Loading environment
	auto etrafo = load_csv_file<double>(folder + "etrafo.csv");
	Eigen::Matrix<double, 4, 4> etrafom;
	for(int i = 0; i < 4; i++) 
		for(int j = 0; j < 4; j ++)
			etrafom(i,j) = etrafo[i][j];
	loadEnv(names[0], etrafom);

	//Loading robot
	auto rtrafo = load_csv_file<double>(folder + "rtrafo.csv");
	Eigen::Matrix<double, 4, 4> rtrafom;
	double scaling = rtrafo[0][0];
	for(int i = 0; i < 4; i++) 
		for(int j = 0; j < 4; j ++)
			rtrafom(i,j) = rtrafo[i+1][j];
	loadRobot(names[1], rtrafom, scaling);

	//Setting up space information and validity checker
	space = std::make_shared<ompl::base::RealVectorStateSpace>(jointCount);
	bounds = std::make_shared<ompl::base::RealVectorBounds>(jointCount);
	for(int i = 0; i < jointCount; i++) {
		bounds->setHigh(i, qmax[i]);
		bounds->setLow(i, qmin[i]);
	}
	bounds->check();
	space->setBounds(*bounds);

	si = std::make_shared<ompl::base::SpaceInformation>(space);
	si->setStateValidityCheckingResolution(rrt_checking_res);
	motion_validator = std::make_shared<ompl::base::DiscreteMotionValidator>(si);
	si->setMotionValidator(motion_validator);

	pdef = std::make_shared<ompl::base::ProblemDefinition>(si);

	//Loading start state
	qstart = flatten(load_csv_file<double>(folder + "qstart.csv"));
	auto start = space->allocState()->as<ompl::base::RealVectorStateSpace::StateType>();
	for(int i = 0; i < jointCount; i ++) start->values[i] = qstart[i];
	pdef->addStartState(start);

	if(names[2] == "position") {
		auto tgoal = flatten(load_csv_file<double>(folder + "qgoal.csv"));
		for(int i = 0; i < 3; i++) goal[i] = tgoal[i];
		using_goal = true;
	} else if(names[2] == "state") {
		qgoal = flatten(load_csv_file<double>(folder + "qgoal.csv"));
		auto sgoal = space->allocState()->as<ompl::base::RealVectorStateSpace::StateType>();
		for(int i = 0; i < jointCount; i ++) sgoal->values[i] = qgoal[i];
		pdef->setGoalState(sgoal);
	}

	return 0;
}

void savePathTo(ompl::geometric::PathGeometric path, std::string filename, 
		double setup_duration, double solve_duration, ompl::base::PlannerStatus solved,
		double solve_prob) {
	std::ofstream filetime(filename+"time.csv");
	filetime << setup_duration << "," << solve_duration << "," << solve_prob;
	filetime.close();
	std::ofstream file(filename+".csv");
	std::ofstream filestate(filename+"state.csv");
	Eigen::Matrix<double, -1, 1> pose(jointCount, 1);
	for(auto state : path.getStates()) {
		auto *rvstate = state->as<ompl::base::RealVectorStateSpace::StateType>();
		for(int i = 0; i < jointCount; i ++) {
			filestate << rvstate->values[i] << ",";
			pose(i, 0) = rvstate->values[i];
		}
		filestate << 0 << std::endl;
		auto v = pose_to_world(pose);
		if(0) {
			if(state == path.getStates().front()) {
				file << v[0] << "," << v[1] << "," << v[2] << std::endl;
				file << v[0]+0.1 << "," << v[1] << "," << v[2] << std::endl;
				file << v[0] << "," << v[1]+0.1 << "," << v[2] << std::endl;
				file << v[0]+0.1 << "," << v[1]+0.1 << "," << v[2] << std::endl;
				file << v[0] << "," << v[1] << "," << v[2]+0.1 << std::endl;
				file << v[0]+0.1 << "," << v[1] << "," << v[2]+0.1 << std::endl;
				file << v[0] << "," << v[1]+0.1 << "," << v[2]+0.1 << std::endl;
				file << v[0]+0.1 << "," << v[1]+0.1 << "," << v[2]+0.1 << std::endl;
				file << v[0] << "," << v[1] << "," << v[2] << std::endl;
				file << v[0] << "," << v[1]+0.1 << "," << v[2]+0.1 << std::endl;
				file << v[0]+0.1 << "," << v[1] << "," << v[2] << std::endl;
				file << v[0]+0.1 << "," << v[1]+0.1 << "," << v[2]+0.1 << std::endl;
				file << v[0] << "," << v[1] << "," << v[2]+0.1 << std::endl;
				file << v[0]+0.1 << "," << v[1] << "," << v[2]+0.1 << std::endl;
				file << v[0] << "," << v[1]+0.1 << "," << v[2] << std::endl;
				file << v[0]+0.1 << "," << v[1]+0.1 << "," << v[2] << std::endl;
			}
		} else {
			file << v[0] << "," << v[1] << "," << v[2] << std::endl;
		}
	}
	file.close();
	filestate.close();
	int npath = path.getStateCount();
	int npose = std::min(intermediate_poses, npath - 1);
	int step = (!npose) ? npath : (npath-1)/npose;
	Mesh temp, full;
	for(int i = 0; i < npath; i += step) {
		if(i + step > npath) i = npath - 1;
		auto *rvstate = path.getStates()[i]->as<ompl::base::RealVectorStateSpace::StateType>();
		for(int j = 0; j < jointCount; j ++) pose(j, 0) = rvstate->values[j];
		temp = *robots[0]->getMesh(pose);
		full.merge(&temp);
	}
	full.saveTo(filename+"int");
}

int main(int argc, char ** argv) {

	loadMetaVariables("metavariables");

	loadProblem(argv[1]);
	


	double hierar_time, total_time, average_duration, duration, setup_duration;
	si->setStateValidityChecker(isStateValid);

	{
		auto setup_start = std::chrono::high_resolution_clock::now();

		ompl::HierarchicalPlanner* planner = new ompl::HierarchicalPlanner(
			si, jointCount, checking_res, new_sample_rate, invertibility_lambda,
			polytope_rootstate_mindist, job_time);
		planner->setVisualize(visualize);
		planner->setVerbose(verbose);
		planner->setProblemDefinition(pdef);
		planner->setEnvironment(workspace, obstacles, partitionMaxSize, volumePoints);
		planner->setPoseToWorld(pose_to_world, pose_to_world_d);
		planner->setIsQValid(isQValid);
		planner->setMaxThreads(nthreads);
		if(using_goal) planner->setEndEffectorGoal(goal);
		planner->setup();
		planner->setupGoal();
		
		auto setup_stop = std::chrono::high_resolution_clock::now();
		setup_duration = double(std::chrono::duration_cast<std::chrono::microseconds>(
				setup_stop - setup_start).count())/1000000.0;

		std::cout << "Using Hierarchical Planner" << std::endl;
		int solves = 0;
		auto start = std::chrono::high_resolution_clock::now();
		ompl::base::PlannerStatus solved;
		for(int i = 0; i < solveCount; i ++) {
			solved = planner->ompl::base::Planner::solve(time_limit);
			if(solved == ompl::base::PlannerStatus::EXACT_SOLUTION) solves ++;
		}
		auto stop = std::chrono::high_resolution_clock::now();
		double solve_prob = double(solves) / double(solveCount);
		duration = double(std::chrono::duration_cast<std::chrono::microseconds>(
						stop - start).count())/1000000.0;
		average_duration = duration/double(solveCount);
		std::cout << "Setup duration : " << setup_duration <<  " seconds " << std::endl;
		std::cout << "Duration : " << duration <<  " seconds " << std::endl;
		std::cout << "Average duration : " << average_duration <<  " seconds " << std::endl;
		total_time = average_duration + setup_duration;
		hierar_time = total_time;

		if (solved) {
			ompl::base::PathPtr path = pdef->getSolutionPath();
			auto gpath = path->as<ompl::geometric::PathGeometric>();
			auto nnn = gpath->getStateCount();
			std::cout << "Solution has " << nnn << " states" << std::endl;
			savePathTo(*gpath, "../visualize/vis/hpath", setup_duration, average_duration, solved, solve_prob);
		}
		else std::cout << "No solution found" << std::endl;
		std::cout << std::endl << std::endl;
	}



	pdef->clearSolutionPaths();


	{
		auto setup_start = std::chrono::high_resolution_clock::now();

		auto rrtplanner(std::make_shared<ompl::geometric::RRTstar>(si));
		rrtplanner->setProblemDefinition(pdef);
		rrtplanner->setRange(rrt_motion_range);
		rrtplanner->setup();

		auto setup_stop = std::chrono::high_resolution_clock::now();
		setup_duration = double(std::chrono::duration_cast<std::chrono::microseconds>(
				setup_stop - setup_start).count())/1000000.0;

		std::cout << "Using RRTstar Planner" << std::endl;
		int solves = 0;
		auto start = std::chrono::high_resolution_clock::now();
		ompl::base::PlannerStatus solved;
		for(int i = 0; i < solveCount; i ++) {
			solved = rrtplanner->ompl::base::Planner::solve(hierar_time);
			if(solved == ompl::base::PlannerStatus::EXACT_SOLUTION) solves ++;
			rrtplanner->clear();
		}
		auto stop = std::chrono::high_resolution_clock::now();
		double solve_prob = double(solves) / double(solveCount);
		duration = double(std::chrono::duration_cast<std::chrono::microseconds>(
						stop - start).count())/1000000.0;
		average_duration = duration/double(solveCount);
		std::cout << "Setup duration : " << setup_duration <<  " seconds " << std::endl;
		std::cout << "Duration : " << duration <<  " seconds " << std::endl;
		std::cout << "Average duration : " << average_duration <<  " seconds " << std::endl;
		total_time = average_duration + setup_duration;

		if (solved) {
			ompl::base::PathPtr path = pdef->getSolutionPath();
			auto gpath = path->as<ompl::geometric::PathGeometric>();
			auto nnn = gpath->getStateCount();
			std::cout << "Solution has " << nnn << " states" << std::endl;
			savePathTo(*gpath, "../visualize/vis/spath", setup_duration, average_duration, solved, solve_prob);
		} else std::cout << "No solution found" << std::endl;
		std::cout << std::endl << std::endl;
	}
	pdef->clearSolutionPaths();

	{
		auto setup_start = std::chrono::high_resolution_clock::now();

		auto rrtplanner(std::make_shared<ompl::geometric::RRTConnect>(si, true));
		rrtplanner->setProblemDefinition(pdef);
		rrtplanner->setup();
		rrtplanner->setRange(rrt_motion_range);

		auto setup_stop = std::chrono::high_resolution_clock::now();
		setup_duration = double(std::chrono::duration_cast<std::chrono::microseconds>(
				setup_stop - setup_start).count())/1000000.0;

		std::cout << "Using RRTconnect Planner" << std::endl;
		int solves = 0;
		auto start = std::chrono::high_resolution_clock::now();
		ompl::base::PlannerStatus solved;
		for(int i = 0; i < solveCount; i ++) {
			solved = rrtplanner->ompl::base::Planner::solve(time_limit);
			if(solved == ompl::base::PlannerStatus::EXACT_SOLUTION) solves ++;
			rrtplanner->clear();
		}
		auto stop = std::chrono::high_resolution_clock::now();
		double solve_prob = double(solves) / double(solveCount);
		duration = double(std::chrono::duration_cast<std::chrono::microseconds>(
						stop - start).count())/1000000.0;
		average_duration = duration/double(solveCount);
		std::cout << "Setup duration : " << setup_duration <<  " seconds " << std::endl;
		std::cout << "Duration : " << duration <<  " seconds " << std::endl;
		std::cout << "Average duration : " << average_duration <<  " seconds " << std::endl;
		total_time = average_duration + setup_duration;

		if (solved) {
			ompl::base::PathPtr path = pdef->getSolutionPath();
			auto gpath = path->as<ompl::geometric::PathGeometric>();
			auto nnn = gpath->getStateCount();
			std::cout << "Solution has " << nnn << " states" << std::endl;
			savePathTo(*gpath, "../visualize/vis/rpath", setup_duration, average_duration, solved, solve_prob);
		} else std::cout << "No solution found" << std::endl;
		std::cout << std::endl << std::endl;
	}
	pdef->clearSolutionPaths();


	{
		auto setup_start = std::chrono::high_resolution_clock::now();

		auto rrtplanner(std::make_shared<ompl::geometric::RRTstar>(si));
		rrtplanner->setFocusSearch(true);
		rrtplanner->setProblemDefinition(pdef);
		rrtplanner->setRange(rrt_motion_range);
		rrtplanner->setup();

		auto setup_stop = std::chrono::high_resolution_clock::now();
		setup_duration = double(std::chrono::duration_cast<std::chrono::microseconds>(
				setup_stop - setup_start).count())/1000000.0;

		std::cout << "Using RRTstar Planner (long time)" << std::endl;
		int solves = 0;
		auto start = std::chrono::high_resolution_clock::now();
		ompl::base::PlannerStatus solved;

		solved = rrtplanner->ompl::base::Planner::solve(optimalSolutionTime);

		auto stop = std::chrono::high_resolution_clock::now();
		double solve_prob;
		if(solved == ompl::base::PlannerStatus::EXACT_SOLUTION) solve_prob = 1;
		else solve_prob = 0;
		duration = double(std::chrono::duration_cast<std::chrono::microseconds>(
						stop - start).count())/1000000.0;
		average_duration = duration;
		std::cout << "Setup duration : " << setup_duration <<  " seconds " << std::endl;
		std::cout << "Duration : " << duration <<  " seconds " << std::endl;
		std::cout << "Average duration : " << average_duration <<  " seconds " << std::endl;
		total_time = average_duration + setup_duration;

		if (solved) {
			ompl::base::PathPtr path = pdef->getSolutionPath();
			auto gpath = path->as<ompl::geometric::PathGeometric>();
			auto nnn = gpath->getStateCount();
			std::cout << "Solution has " << nnn << " states" << std::endl;
			savePathTo(*gpath, "../visualize/vis/bpath", setup_duration, average_duration, solved, solve_prob);
		} else std::cout << "No solution found" << std::endl;
		std::cout << std::endl << std::endl;
	}
	return 0;
}
