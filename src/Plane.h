#ifndef PLANE_H
#define PLANE_H

#include <eigen3/Eigen/Eigen>

class Plane {
	/*
	Class to store a plane and speed up seperation of points 
	by the plane in 3D and speed up intersectin of segment between 
	two points and plane

	Attributes
	----------
	N : vector
		normal to the plane
	T : double
		solution to x = dot(N,Y) for Y in the considered plane
	on_plane : list
		list of points that are on the plane (after seperation 
		and intersection queries)

	Methods
	-------
		reset_on_plane()
		resets the on_plane variable
	*/
	public:
		Eigen::Matrix<double, 3, 1> N;
		double T;
		bool is_degenerate;

		std::vector<int> on_plane, left, right;

		std::vector<std::vector<std::pair<int, int>>> inters;
		std::vector<bool> on_plane_inter;

		std::vector<Eigen::Matrix<double, 3, 1>>* L; //This pointer does not belong to this object

		Plane(std::vector<Eigen::Matrix<double, 3, 1>> V);

		void prepare_right_left_queries(std::vector<Eigen::Matrix<double, 3, 1>>* L);
		bool is_left(int i);
		bool is_right(int i);

		void prepare_intersection_queries(std::vector<Eigen::Matrix<double, 3, 1>>* L);
		int intersect(int ia, int ib);

		void calc(int i);

		void reset_on_plane();
};
#endif
