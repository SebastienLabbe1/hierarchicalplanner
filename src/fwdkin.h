#ifndef FWDKIN_H
#define FWDKIN_H

#include <eigen3/Eigen/Eigen>
#include <math.h>

//This code was inspired by code from Constantin Dresel

template<typename T>
Eigen::Matrix<T, 4, 4> gen_trafo_z_shift(T z_shift) {
	//Returns a z shift matrix of a 3D homogeneous transformation
	Eigen::Matrix<T, 4, 4> sol;
	sol << 1.0, .0, .0, .0, 
		.0, 1.0, .0, .0,
		.0, .0, 1.0, z_shift,
		.0, .0, .0, 1.0;
	return sol;
}

template<typename T>
Eigen::Matrix<T, 4, 4> gen_dh_trafo_d4(T a, T alpha, T theta, T b) {
	//Return the DH homogeneous transformation matrix differentiated 
	// with respect to the variable b
	Eigen::Matrix<T, 4, 4> sol;
	sol << .0, .0, .0, .0,
		.0, .0, .0, .0,
		.0, .0, .0, 1.0,
		.0, .0, .0, .0;
	return sol;
}

template<typename T>
Eigen::Matrix<T, 4, 4> gen_dh_trafo_d3(T a, T alpha, T theta, T b) {
	//Return the DH homogeneous transformation matrix differentiated 
	// with respect to the variable theta
	T cos_alpha = cos(alpha);
	T sin_alpha = sin(alpha);
	T cos_theta = cos(theta);
	T sin_theta = sin(theta);
	Eigen::Matrix<T, 4, 4> sol;
	sol << - sin_theta, - cos_theta * cos_alpha, 
		cos_theta * sin_alpha, - a * sin_theta,
		
		cos_theta, - sin_theta * cos_alpha,
		sin_theta * sin_alpha, a * cos_theta,

		.0, .0, .0, .0,
		
		.0, .0, .0, .0;
	return sol;
}

template<typename T>
Eigen::Matrix<T, 4, 4> gen_dh_trafo_d2(T a, T alpha, T theta, T b) {
	//Return the DH homogeneous transformation matrix differentiated 
	// with respect to the variable alpha
	T cos_alpha = cos(alpha);
	T sin_alpha = sin(alpha);
	T cos_theta = cos(theta);
	T sin_theta = sin(theta);
	Eigen::Matrix<T, 4, 4> sol;
	sol << 0, sin_theta * sin_alpha, 
		sin_theta * cos_alpha, 0,
		
		0, - cos_theta * sin_alpha,
		- cos_theta * cos_alpha, 0,

		.0, cos_alpha, - sin_alpha, 0,
		
		.0, .0, .0, .0;
	return sol;
}

template<typename T>
Eigen::Matrix<T, 4, 4> gen_dh_trafo(T a, T alpha, T theta, T b) {
	//Return the DH homogeneous transformation matrix
	T cos_alpha = cos(alpha);
	T sin_alpha = sin(alpha);
	T cos_theta = cos(theta);
	T sin_theta = sin(theta);
	Eigen::Matrix<T, 4, 4> sol;
	sol << cos_theta, - sin_theta * cos_alpha, 
		sin_theta * sin_alpha, a * cos_theta,
		
		sin_theta, cos_theta * cos_alpha,
		- cos_theta * sin_alpha, a * sin_theta,

		.0, sin_alpha, cos_alpha, b,
		
		.0, .0, .0, 1.0;
	return sol;
}

template<typename T>
Eigen::Matrix<T, 4, 4> fwdkin_hom(std::vector<T> q,
		std::vector<std::vector<double>> dh_ext, std::vector<T> jt, 
		Eigen::Matrix<T, 4, 4> base_to_world_trafo) {
	//Return the homogeneous transformation matrix of the end effector 
	// of the robot described by dh_ext, jt and joint values q
	Eigen::Matrix<T, 4, 4> trafo_a = base_to_world_trafo;
	int dh_tab_size = dh_ext.size();
	if(dh_tab_size > 0) {
		for(int idx = 0; idx < dh_tab_size; idx ++) {
			Eigen::Matrix<T, 4, 4> trafo = gen_dh_trafo(dh_ext[idx][0], 
						dh_ext[idx][1] + T((jt[idx] == 2) ? q[idx] : 0),
						dh_ext[idx][2] + T((jt[idx] == 0) ? q[idx] : 0), 
						dh_ext[idx][3] + T((jt[idx] == 1) ? q[idx] : 0));
			trafo_a *= trafo;
		}
		trafo_a *= gen_trafo_z_shift(dh_ext[dh_tab_size-1][5]);
	}
	return trafo_a;
}

template<typename T>
Eigen::Matrix<T, 3, 1> fwdkin_hom_mod(Eigen::Matrix<double, Eigen::Dynamic, 1> q,
		std::vector<std::vector<double>> dh_ext, std::vector<T> jt, 
		Eigen::Matrix<T, 4, 4> base_to_world_trafo) {
	//Return the position in 3D of the end effector 
	// of the robot described by dh_ext, jt and joint values q
	Eigen::Matrix<T, 4, 4> trafo_a = base_to_world_trafo;
	int dh_tab_size = dh_ext.size();
	if(dh_tab_size > 0) {
		for(int idx = 0; idx < dh_tab_size; idx ++) {
			Eigen::Matrix<T, 4, 4> trafo = gen_dh_trafo(dh_ext[idx][0], 
						dh_ext[idx][1] + T((jt[idx] == 2) ? q(idx,0) : 0),
						dh_ext[idx][2] + T((jt[idx] == 0) ? q(idx,0) : 0), 
						dh_ext[idx][3] + T((jt[idx] == 1) ? q(idx,0) : 0));
			trafo_a *= trafo;
		}
		trafo_a *= gen_trafo_z_shift(dh_ext[dh_tab_size-1][5]);
	}
	return trafo_a.block(0,3,3,1);
}

template<typename T>
std::vector<Eigen::Matrix<T, 4, 4>> fwdkin_hom_full(std::vector<T> q,
		std::vector<std::vector<double>> dh_ext, std::vector<T> jt, 
		Eigen::Matrix<T, 4, 4> base_to_world_trafo) {
	//Return the homogeneous transformation matrices of each joint 
	//including end effector of the robot described by dh_ext, jt 
	//and joint values q
	Eigen::Matrix<T, 4, 4> trafo_a = base_to_world_trafo;
	std::vector<Eigen::Matrix<T, 4, 4>> all_trafos = {trafo_a};
	int dh_tab_size = dh_ext.size();
	if(dh_tab_size > 0) {
		for(int idx = 0; idx < dh_tab_size; idx ++) {
			Eigen::Matrix<T, 4, 4> trafo = gen_dh_trafo(dh_ext[idx][0], 
				dh_ext[idx][1] + T((jt[idx] == 2) ? q[idx] : 0),
				dh_ext[idx][2] + T((jt[idx] == 0) ? q[idx] : 0), 
				dh_ext[idx][3] + T((jt[idx] == 1) ? q[idx] : 0));
			trafo_a = trafo_a * trafo;
			all_trafos.push_back(trafo_a * gen_trafo_z_shift(dh_ext[idx][5]));
		}
	}
	return all_trafos;
}
template<typename T>
std::vector<Eigen::Matrix<T, 4, 4>> fwdkin_hom_full(
		Eigen::Matrix<double, Eigen::Dynamic, 1> q,
		std::vector<std::vector<double>> dh_ext, std::vector<T> jt, 
		Eigen::Matrix<T, 4, 4> base_to_world_trafo) {
	//Return the homogeneous transformation matrices of each joint 
	//including end effector of the robot described by dh_ext, jt 
	//and joint values q
	Eigen::Matrix<T, 4, 4> trafo_a = base_to_world_trafo;
	std::vector<Eigen::Matrix<T, 4, 4>> all_trafos = {trafo_a};
	int dh_tab_size = dh_ext.size();
	if(dh_tab_size > 0) {
		for(int idx = 0; idx < dh_tab_size; idx ++) {
			Eigen::Matrix<T, 4, 4> trafo = gen_dh_trafo(dh_ext[idx][0], 
				dh_ext[idx][1] + T((jt[idx] == 2) ? q(idx,0) : 0),
				dh_ext[idx][2] + T((jt[idx] == 0) ? q(idx,0) : 0), 
				dh_ext[idx][3] + T((jt[idx] == 1) ? q(idx,0) : 0));
			trafo_a = trafo_a * trafo;
			all_trafos.push_back(trafo_a * gen_trafo_z_shift(dh_ext[idx][5]));
		}
	}
	return all_trafos;
}
template<typename T>
Eigen::Matrix<T, 3, -1> fwdkin_hom_full_mod(
		Eigen::Matrix<double, Eigen::Dynamic, 1> q,
		std::vector<std::vector<double>> dh_ext, std::vector<T> jt, 
		Eigen::Matrix<T, 4, 4> base_to_world_trafo) {
	//Return the positions of each joint 
	//including end effector of the robot described by dh_ext, jt 
	//and joint values q
	Eigen::Matrix<T, 4, 4> trafo_a = base_to_world_trafo;
	std::vector<Eigen::Matrix<T, 4, 4>> all_trafos = {trafo_a};
	int dh_tab_size = dh_ext.size();
	if(dh_tab_size > 0) {
		for(int idx = 0; idx < dh_tab_size; idx ++) {
			Eigen::Matrix<T, 4, 4> trafo = gen_dh_trafo(dh_ext[idx][0], 
				dh_ext[idx][1] + T((jt[idx] == 2) ? q(idx,0) : 0),
				dh_ext[idx][2] + T((jt[idx] == 0) ? q(idx,0) : 0), 
				dh_ext[idx][3] + T((jt[idx] == 1) ? q(idx,0) : 0));
			trafo_a = trafo_a * trafo;
			all_trafos.push_back(trafo_a * gen_trafo_z_shift(dh_ext[idx][5]));
		}
	}
	Eigen::Matrix<T, 3, -1> temp;
	temp.resize(3, all_trafos.size());
	int i = 0;
	for(auto tr : all_trafos) {
		temp.block(0, i, 3, 1) = tr.block(0, 3, 3, 1);
		i++;
	}
	return temp;
}

template<typename T>
std::vector<Eigen::Matrix<T, 4, 4>> fwdkin_hom_d(
		std::vector<T> q,
		std::vector<std::vector<double>> dh_ext, std::vector<T> jt, 
		Eigen::Matrix<T, 4, 4> base_to_world_trafo) {
	//Return the list of Jacobian matrices of the end effector position
	//with respect to each link
	Eigen::Matrix<T, 4, 4> trafo_a = base_to_world_trafo;
	std::vector<Eigen::Matrix<T, 4, 4>> all_trafos = {trafo_a};
	int dh_tab_size = dh_ext.size();
	if(dh_tab_size > 0) {
		for(int idx = 0; idx < dh_tab_size; idx ++) {
			if(idx < dh_tab_size - 1) all_trafos.push_back(all_trafos.back());
			Eigen::Matrix<T, 4, 4> trafo = gen_dh_trafo(dh_ext[idx][0],
						dh_ext[idx][1] + T((jt[idx] == 2) ? q[idx] : 0),
						dh_ext[idx][2] + T((jt[idx] == 0) ? q[idx] : 0), 
						dh_ext[idx][3] + T((jt[idx] == 1) ? q[idx] : 0));
			Eigen::Matrix<T, 4, 4> trafo_d;
			if(jt[idx] == 1) {
				trafo_d = gen_dh_trafo_d4(dh_ext[idx][0],
						dh_ext[idx][1],
						dh_ext[idx][2], 
						dh_ext[idx][3] + T(q[idx]));
			} else if(jt[idx] == 0) {
				trafo_d = gen_dh_trafo_d3(dh_ext[idx][0],
						dh_ext[idx][1],
						dh_ext[idx][2] + T(q[idx]), 
						dh_ext[idx][3]);
			} else if(jt[idx] == 2) {
				trafo_d = gen_dh_trafo_d2(dh_ext[idx][0],
						dh_ext[idx][1] + T(q[idx]),
						dh_ext[idx][2], 
						dh_ext[idx][3]);
			} else {
				throw std::runtime_error("no derivative found");
			}
			for(int jdx = 0; jdx < idx; jdx ++) {
				all_trafos[jdx] *= trafo;
			}
			all_trafos[idx] *= trafo_d;
			if(idx + 1 < dh_tab_size) all_trafos[idx + 1] *= trafo;
		}
	}
	return all_trafos;
}

template<typename T>
Eigen::Matrix<T, 3, Eigen::Dynamic> fwdkin_hom_mod_d(
		Eigen::Matrix<double, Eigen::Dynamic, 1> q,
		std::vector<std::vector<double>> dh_ext, std::vector<T> jt, 
		Eigen::Matrix<T, 4, 4> base_to_world_trafo) {
	//Return Jacobian of the function giving the end effector position 
	//from link values vector
	Eigen::Matrix<T, 4, 4> trafo_a = base_to_world_trafo;
	std::vector<Eigen::Matrix<T, 4, 4>> all_trafos = {trafo_a};
	int dh_tab_size = dh_ext.size();
	if(dh_tab_size > 0) {
		for(int idx = 0; idx < dh_tab_size; idx ++) {
			if(idx < dh_tab_size - 1) all_trafos.push_back(all_trafos.back());
			Eigen::Matrix<T, 4, 4> trafo = gen_dh_trafo(dh_ext[idx][0],
						dh_ext[idx][1] + T((jt[idx] == 2) ? q(idx,0) : 0),
						dh_ext[idx][2] + T((jt[idx] == 0) ? q(idx,0) : 0), 
						dh_ext[idx][3] + T((jt[idx] == 1) ? q(idx,0) : 0));
			Eigen::Matrix<T, 4, 4> trafo_d;
			if(jt[idx] == 1) {
				trafo_d = gen_dh_trafo_d4(dh_ext[idx][0],
						dh_ext[idx][1],
						dh_ext[idx][2], 
						dh_ext[idx][3] + T(q(idx,0)));
			} else if(jt[idx] == 0) {
				trafo_d = gen_dh_trafo_d3(dh_ext[idx][0],
						dh_ext[idx][1],
						dh_ext[idx][2] + T(q(idx,0)), 
						dh_ext[idx][3]);
			} else if(jt[idx] == 2) {
				trafo_d = gen_dh_trafo_d2(dh_ext[idx][0],
						dh_ext[idx][1] + T(q(idx,0)),
						dh_ext[idx][2], 
						dh_ext[idx][3]);
			} else {
				throw std::runtime_error("no derivative found");
			}
			for(int jdx = 0; jdx < idx; jdx ++) {
				all_trafos[jdx] *= trafo;
			}
			all_trafos[idx] *= trafo_d;
			if(idx + 1 < dh_tab_size) {
				all_trafos[idx + 1] *= trafo;
			}
		}
	}
	Eigen::Matrix<T, 3, Eigen::Dynamic> J;
	J.resize(3, jt.size());
	for(unsigned int i = 0; i < jt.size(); i ++) {
		J.block(0,i,3,1) = all_trafos[i].block(0,3,3,1);
	}
	return J;
}

#endif
