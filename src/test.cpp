#include "test.h"
#include <ompl/base/SpaceInformation.h>
#include <ompl/base/spaces/SE3StateSpace.h>
#include <ompl/geometric/planners/rrt/RRTConnect.h>
#include <ompl/geometric/SimpleSetup.h>

#include <ompl/config.h>
#include <iostream>

#include "HierarchicalPlanner.h"
#include "utils.h"
#include "Robot.h"
#include "Mesh.h"
#include "test.h"
#include "fwdkin.h"

void test_triangulate() {
	std::vector<Eigen::Matrix<double, 3, 1>> tris;
	Eigen::Matrix<double, 3, 1> tri;
	tri << 0, 0, 0;
	tris.push_back(tri);
	tri << 0, 0, 1;
	tris.push_back(tri);
	tri << 0, 0, 2;
	tris.push_back(tri);
	tri << 0, 1, 2;
	tris.push_back(tri);
	tri << 0, 2, 2;
	tris.push_back(tri);
	tri << 0, 2, 1;
	tris.push_back(tri);
	tri << 0, 2, 0;
	tris.push_back(tri);
	tri << 0, 1, 0;
	tris.push_back(tri);
	tri << 1, 0, 0;
	std::vector<std::vector<int>> cut = triangulate({0,1,2,3,4,5,6,7}, tris, tri) ;
	for(auto vec : cut) {
		for(auto i : vec) {
			std::cout << i << " ";
		}
		std::cout << std::endl;	
	}
}

Eigen::Matrix<double, 3, Eigen::Dynamic> state_to_links(const ompl::base::State *state) {
	/*
	auto *rvstate = state->as<ompl::base::RealVectorStateSpace::StateType>();
	Eigen::Matrix<double, -1, 1> q(N, 1);
	for(int i = 0; i < N; i ++) q(i, 0) = rvstate->values[i];
	irobm.lock();
	int i = irob;
	irob = (irob + 1) % NROBS;
	std::cout << " hello " << std::endl;
	irobm.unlock();
	const std::lock_guard<std::mutex> lock(robsm[i]);
	robs[i]->update_pose(q);
	std::cout << " hello " << std::endl;
	for(int j = 0; j < int(robs[i]->collision_objects.size()); j ++)
		std::cout << robs[i]->collision_objects[j].collisionGeometry()->aabb_center << std::endl;
	for(int j = 0; j < int(robs[i]->collision_objects.size()); j ++) {
		if(robs[i]->link_empty[j]) continue;
		robs[i]->collision_objects[j].computeAABB();
		auto aabb = robs[i]->collision_objects[j].getAABB();
		auto c = aabb.center();
		std::cout << c << std::endl;
	}
	std::cout << " hello " << std::endl;
	std::cout << " pose " << q.transpose() << std::endl;

	for(int j = 1; j < 20; j ++) {
		auto env = meshs_to_coll<double>({*stl_to_mesh("tests/test" + std::to_string(j) + ".stl")});
		if(robs[i]->check_collision(env)) {
			std::cout << " coll with test " << j << std::endl;
		} else {
			std::cout << " no coll with test " << j << std::endl;
		}
	}


	*/
	Eigen::Matrix<double, 3, -1> p;
	return p;
}

int loadRealRobot() {
	/*
	std::vector<double> q_data = flatten(load_csv_file<double>("data/q.csv"));
	std::vector<std::vector<double>> dh_tab_data = load_csv_file<double>("data/DHext.csv");
	std::vector<double> jt_data = flatten(load_csv_file<double>("data/jt.csv"));
	std::vector<std::vector<double>> capsules_tab = load_csv_file<double>("data/Capsules.csv");
	std::vector<std::vector<double>> boxes_tab = load_csv_file<double>("data/Boxes.csv");
	std::vector<std::vector<double>> vertices_tab = load_csv_file<double>("data/Vertices.csv");
	std::vector<std::vector<double>> vertices_size_tab = load_csv_file<double>("data/vertices_counts.csv");
	std::vector<std::vector<double>> vertices_tabs;
	int num_point_clouds = vertices_size_tab.size();
	int next, last = 0;
	for(int cloud_idx = 0; cloud_idx < num_point_clouds; cloud_idx ++) {
		next = vertices_size_tab[0][cloud_idx];
		vertices_tabs.push_back(std::vector<double>(&vertices_tab[0][last], &vertices_tab[0][next]));
		last = next;
	}
	Eigen::Matrix<double, 3, 3> orient;
	orient << 1.0, .0, .0, .0, 1.0, .0, .0, .0, 1.0;
	Eigen::Matrix<double, 3, 1> position;
	position << .0, .0, .0;
	rob = new MeshRobot(dh_tab_data, jt_data, q_data, vertices_tabs, position, orient);
	*/
	return 1;
}

void testPositions() {
	/*
	std::vector<double> q_startf = {0, 0, 0};
	double x = 2;
	std::vector<double> q_goalf = {double(M_PI) / 2 + x, double(M_PI) - x, 0};

	std::cout << "Testing position ======================================" << std::endl;

	if(robs[0]->check_self_collision()) std::cout << "test 1 collide " << std::endl;
	else std::cout << "test 1 no collide " << std::endl;

	robs[0]->update_pose(q_startf);

	if(robs[0]->check_self_collision()) std::cout << "test start collide " << std::endl;
	else std::cout << "test start no collide " << std::endl;

	robs[0]->update_pose(q_goalf);

	if(robs[0]->check_self_collision()) std::cout << "test goal collide " << std::endl;
	else std::cout << "test goal no collide " << std::endl;

	std::cout << "Testing positions done ======================================" << std::endl;
	*/
}
