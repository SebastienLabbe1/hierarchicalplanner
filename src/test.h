#ifndef TEST_H
#define TEST_H

#include <eigen3/Eigen/Dense>

#include <ompl/base/SpaceInformation.h>
#include <ompl/base/spaces/SE3StateSpace.h>
#include <ompl/geometric/planners/rrt/RRTConnect.h>
#include <ompl/geometric/SimpleSetup.h>
#include <ompl/config.h>

void test_triangulate();

Eigen::Matrix<double, 3, Eigen::Dynamic> state_to_links(const ompl::base::State *state);

int loadRealRobot();

void testPositions();

#endif
