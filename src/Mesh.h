#ifndef MESH_H
#define MESH_H

#include <vector>
#include <eigen3/Eigen/Eigen>

class Mesh {
	private:
	public:
		std::vector<Eigen::Matrix<double, 3, 1>> verts;
		std::vector<std::vector<int>> faces;

		Eigen::Matrix<double, 3, 1> shift;
		Eigen::Matrix<double, 3, 3> rotation;

		Mesh(std::vector<Eigen::Matrix<double, 3, 1>> verts = {}, std::vector<std::vector<int>> faces = {}, 
			Eigen::Matrix<double, 3, 1> shift = Eigen::Matrix<double, 3, 1>::Zero(), 
			Eigen::Matrix<double, 3, 3> rotation = Eigen::Matrix<double, 3, 3>::Identity());
		~Mesh();

		void setShift(Eigen::Matrix<double, 3, 1> shift);
		void setSize(double size);
		void setScale(double sx, double sy, double sz);
		void setTransformation(Eigen::Matrix<double, 4, 4> trafo);

		void applyStoredTransformation(void);

		void merge(Mesh* mesh);

		void print(void);
		void saveTo(std::string filename);
		void saveAsLineTo(std::string filename);
};

#endif
