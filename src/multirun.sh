#!/bin/bash
problems="p4 p5 p6 p10 pc1 pc2"
make -j 8
rm ../visualize/tex/*
for prob in $problems; do 
	echo Running problem $prob
	rm ../visualize/vis/* -f
	./prog $prob > /dev/null
	python3 -W ignore ../visualize/pathLengths.py $prob > /dev/null
	python3 -W ignore ../visualize/createTable.py $prob > /dev/null
	echo Finished problem $prob
done
