#ifndef HIERARCHICALPLANNER_H
#define HIERARCHICALPLANNER_H

#include <ompl/geometric/PathGeometric.h>
#include <ompl/base/Planner.h>
#include <ompl/util/RandomNumbers.h>
#include <ompl/tools/config/SelfConfig.h>

#include <fcl/narrowphase/collision.h>
#include <fcl/geometry/bvh/BVH_model.h>
#include <fcl/math/bv/OBBRSS.h>

#include "Mesh.h"
#include <mutex>
#include <atomic>
#include <condition_variable>
#include <thread>

typedef Eigen::Matrix<double, 3, 1> (*pose_to_world_func_ptr)(Eigen::Matrix<double, Eigen::Dynamic, 1> pose);
typedef Eigen::Matrix<double, 3, Eigen::Dynamic> (*pose_to_world_d_func_ptr)(Eigen::Matrix<double, Eigen::Dynamic, 1> pose);
typedef bool (*isQValid_func_ptr)(Eigen::Matrix<double, Eigen::Dynamic, 1> q);


//Here are the node for the 3 trees kept during the Planning

//This node only keeps a pose and its parent
typedef struct Q_node_struct {
	Eigen::Matrix<double, Eigen::Dynamic, 1> q;
	Q_node_struct *parent = nullptr;
} Q_node;

//This node keeps all pose with same endeffector position
// which are reachble by null space movements
typedef struct State_node_struct {
	Eigen::Matrix<double, 3, 1> r;
	std::vector<Q_node*> qs; 
	std::vector<State_node_struct*> children;
	~State_node_struct() {
		for(auto x : children) delete[] x;
		for(auto x : qs) delete[] x;
	}
} State_node;

typedef std::pair<Q_node*, State_node*> QState_node;

//This node keeps the information for the tree grown on 
//the partition adjacency graph
typedef struct Polytope_node_struct {
	int F;
	std::vector<State_node*> ts; 
	std::vector<Polytope_node_struct*> children;
	std::mutex m;
	Polytope_node_struct *parent = nullptr;
	~Polytope_node_struct() {
		for(auto x : children) delete[] x;
	}
} Polytope_node;

//This job is for the scheduler to comunicate with the 
//threads and give local planning jobs
typedef struct Job_struct {
	Polytope_node* polytope_node = nullptr;
	double success_rate = 1;
	double dist_left = 100;
	int goal_partition = 0;
	int nfails = 0;
	int points_explored = 0;
	Job_struct(Polytope_node* tpolytope_node, int tgoal_partition,
		double tsuccess_rate, double tdist_left, int tnfails,
		int tpoints_explored) {
		polytope_node = tpolytope_node;
		goal_partition = tgoal_partition;
		success_rate = tsuccess_rate;
		dist_left = tdist_left;
		nfails = tnfails;
		points_explored = tpoints_explored;
	}
} Job;

bool operator<(const Job& j1, const Job& j2);

class Environment;

namespace ompl
{
	class HierarchicalPlanner : public base::Planner
	{
	private:
		bool is_q_valid_set = false, environment_set = false, 
			pose_to_world_set = false, end_effector_goal_set = false,
			verbose = false, visualize = false, goal_setup = false;
		pose_to_world_func_ptr pose_to_world = nullptr;
		pose_to_world_d_func_ptr pose_to_world_d = nullptr;
		isQValid_func_ptr isQValid = nullptr;

		double max_step_size, new_partition_sample_rate, 
			polytope_rootstate_mindist;

		Eigen::Matrix<double, -1, 1> qmax, qmin, startq;

		Eigen::Matrix<double, 3, 1> goalr, startr;
		int njoints, goalF, startF;

		std::atomic<int> qstep_attemps, states, null_space_states, position_states;
		std::atomic<bool> solved;

		Environment* environment = nullptr;

		std::priority_queue<Job> jobs_queue;
		std::mutex jobs_queue_mutex;

		std::condition_variable thread_condvar, scheduler_condvar;
		std::mutex thread_mutex, scheduler_mutex;

		std::vector<std::vector<Polytope_node*>> partition_root_nodes;
		std::vector<std::mutex> partition_root_nodes_mutex;

		int job_time = 1000000;

		int max_threads = 1;

		double invertibility_lambda = 0.01;

	public:

		HierarchicalPlanner(const base::SpaceInformationPtr &si, int njoints, 
				double max_step_size, double new_partition_sample_rate,
				double invertibility_lambda, double polytope_rootstate_mindist,
				int job_time); 
		virtual ~HierarchicalPlanner(void);

		virtual base::PlannerStatus solve(const base::PlannerTerminationCondition &ptc);
		virtual void getPlannerData(base::PlannerData &data) const;
		virtual void clear(void);
		virtual void setup(void);

		void setMaxThreads(int max_threads);
		void setIsQValid(isQValid_func_ptr isQValid);
		void setEnvironment(Mesh workspace, std::vector<Mesh> obstacles,
					double partitionMaxSize, int volumePoints);
		void setPoseToWorld(pose_to_world_func_ptr pose_to_world, 
					pose_to_world_d_func_ptr pose_to_world_d);
		void setEndEffectorGoal(Eigen::Matrix<double, 3, 1> goal);
		void setVerbose(bool verbose);
		void setVisualize(bool visualize);
		void setupGoal();

		bool isQReallyValid(Eigen::Matrix<double, -1, 1> q);

		Eigen::Matrix<double, -1, 1> state_to_vec(const ompl::base::State *state);
		ompl::base::State* vec_to_state(Eigen::Matrix<double, -1, 1> vec);

		void addAllJobsFrom(Polytope_node *n, Job &job);
		void addJob(Job &job);
		Job getJob(void);

		void threadLoop();
		Eigen::Matrix<double, -1, 1> QStep(Eigen::Matrix<double, -1, 1> q, 
							Eigen::Matrix<double, -1, 1> dq);
		State_node* connectRegion(Job &job);
		QState_node findNear(Polytope_node *n, Eigen::Matrix<double, 3, 1> r);
		State_node* extend(QState_node qtnear, Eigen::Matrix<double, 3, 1> rnew, Polytope_node *n);

		Eigen::Matrix<double, -1, 1> sampleQ();

		void printProblemInfo(void);
		void resetCounters(void);
		void printSolverInfo(void);
	};
}

#endif
