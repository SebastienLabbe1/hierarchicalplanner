#ifndef UTILS_H
#define UTILS_H

#include <string>
#include <fstream>
#include <iostream>
#include <fcl/narrowphase/collision.h>
#include <fcl/geometry/bvh/BVH_model.h>
#include <fcl/math/bv/OBBRSS.h>
#include "Mesh.h"

std::vector<std::string> load_name_file(std::string filename);

Mesh* stl_to_mesh(std::string filename);

template <typename T>
fcl::CollisionObject<T>* mesh_to_coll(const Mesh &mesh) {

	std::shared_ptr<fcl::BVHModel<fcl::OBBRSS<T>>> model = std::make_shared<fcl::BVHModel<fcl::OBBRSS<T>>>();
	model->beginModel();
	for(auto face : mesh.faces) {
		model->addTriangle(mesh.verts[face[0]], mesh.verts[face[1]], mesh.verts[face[2]]); 
	}
	model->endModel();
	fcl::CollisionObject<T>* obj = new fcl::CollisionObject<T>(model);
	return obj;
}

template <typename T>
fcl::CollisionObject<T>* meshs_to_coll(std::vector<Mesh> meshs) {

	std::shared_ptr<fcl::BVHModel<fcl::OBBRSS<T>>> model = std::make_shared<fcl::BVHModel<fcl::OBBRSS<T>>>();
	model->beginModel();
	for(auto mesh : meshs) {
		for(auto face : mesh.faces) {
			model->addTriangle(mesh.verts[face[0]], mesh.verts[face[1]], mesh.verts[face[2]]); 
		}
	}
	model->endModel();
	fcl::CollisionObject<T>* obj = new fcl::CollisionObject<T>(model);
	return obj;
}

template <typename T>
fcl::CollisionObject<T>* stl_to_coll(std::string filename) {
	std::ifstream file (filename.c_str(), std::ios::in);

	if(!file) throw std::runtime_error(filename + " does not exist");
	std::streambuf *cinbuf = std::cin.rdbuf();
	std::cin.rdbuf(file.rdbuf());

	std::string buf;

	std::cin >> buf;
	if(buf != "solid") throw std::runtime_error(filename + " is not proper stl file");

	std::cin >> buf;
	if(buf != "solid") throw std::runtime_error(filename + " is not proper stl file");

	std::shared_ptr<fcl::BVHModel<fcl::OBBRSS<T>>> mesh = std::make_shared<fcl::BVHModel<fcl::OBBRSS<T>>>();
	mesh->beginModel();

	fcl::Vector3<T> v[3];
	fcl::Vector3<T> n;


	std::cin >> buf;
	while(buf == "facet") {
		while(buf != "endfacet") {
			std::cin >> buf;
			if(buf == "normal") {
				std::cin >> n[0] >> n[1] >> n[2];
			} else if (buf == "outer") {
				std::cin >> buf;
				std::cin >> buf;
				std::cin >> v[0][0] >> v[0][1] >> v[0][2];
				std::cin >> buf;
				std::cin >> v[1][0] >> v[1][1] >> v[1][2];
				std::cin >> buf;
				std::cin >> v[2][0] >> v[2][1] >> v[2][2];
			}
		}
		mesh->addTriangle(v[0], v[1], v[2]); 
	}
	mesh->endModel();

	fcl::CollisionObject<T>* obj = new fcl::CollisionObject<T>(mesh);

	return obj;
}

template<typename T>
fcl::CollisionObject<T>* stlb_to_coll(std::string filename) {
	std::ifstream file (filename.c_str(), 
			std::ios::in | std::ios::binary);

	if(!file) return nullptr;


	char header[80] = "";
	char n_tric[4];
	unsigned long n_tri;

	if(!file) return nullptr;
	file.read(header, 80);
	std::cout <<"header: " << header << std::endl;
	std::cout << " HERE =============== " << std::endl;

	if(!file) return nullptr;
	//file.read(n_tric, 4);
	std::cout << " HERE =============== " << std::endl;
	std::cout << n_tric[0] <<n_tric[1] <<n_tric[2] <<n_tric[3] <<std::endl;
	std::cout << " HERE =============== " << std::endl;
	n_tri = *((unsigned long*)n_tric);
	std::cout <<"n tri: " << n_tri << std::endl;

	std::shared_ptr<fcl::BVHModel<fcl::OBBRSS<T>>> mesh = std::make_shared<fcl::BVHModel<fcl::OBBRSS<T>>>();

	char face[50];
	T x, y, z;
	fcl::Vector3<T> p[3];

	for(unsigned int i = 0; i < 10; i++) {
		if(!file) return nullptr;
		file.read(face, 50);

		for(int j = 0; j < 3; j ++) {
			x = *((T *)face + j * 12 + 12);
			y = *((T *)face + j * 12 + 16);
			z = *((T *)face + j * 12 + 20);
			p[j] = {x, y, z};
		}

		mesh->addTriangle(p[0], p[1], p[2]); 
	}
	mesh->endModel();

	fcl::CollisionObject<T>* obj = new fcl::CollisionObject<T>(mesh);

	return obj;
}

template<typename T>
std::vector<T> flatten(std::vector<std::vector<T>> list) {
	std::vector<T> flat;
	for(auto v : list) {
		flat.insert(flat.end(), v.begin(), v.end());
	}
	return flat;
}

/*
template<typename T>
std::vector<std::vector<T>> load_csv_file(std::string filename) {
	FILE * file = fopen(filename.c_str(), "r");
	if(!file) return {};
	std::vector<std::vector<T>> points(1);
	char fill;
	T temp;
	while(0 < std::fscanf(file, "%f%c", &temp, &fill)) {
		points.back().push_back(temp);
		if(fill == '\n') {
			points.push_back({});
		}
	}
	if(points.back().size() == 0) points.pop_back();
	return points;
}
*/

template<typename T>
std::vector<std::vector<T>> load_csv_file(std::string filename) {
	std::ifstream file (filename.c_str(), std::ios::in);
	if(!file) return {};
	std::vector<std::vector<T>> points(1);
	std::string line;
	while(1) {
		line = "";
		file >> line;
		if(line == "") break;
		int last = 0, cur = 0;
		while(last < int(line.size())) {
			if(cur >= int(line.size()) || line[cur] == ',') {
				points.back().push_back(atof(line.substr(last, cur-last).c_str()));
				last = cur + 1;
			}
			cur ++;
		}
		points.push_back({});
	}
	while(!points.back().size()) points.pop_back();
	return points;
}
template<typename T>
std::vector<std::vector<T>> load_scsv_file(std::string filename) {
	std::ifstream file (filename.c_str(), std::ios::in);
	if(!file) return {};
	std::vector<std::vector<T>> points(1);
	std::string line;
	while(1) {
		line = "";
		file >> line;
		if(line == "") break;
		int last = 0, cur = 0;
		while(last < int(line.size())) {
			if(cur >= int(line.size()) || line[cur] == ',') {
				points.back().push_back(T(line.substr(last, cur-last).c_str()));
				last = cur + 1;
			}
			cur ++;
		}
		points.push_back({});
	}
	while(!points.back().size()) points.pop_back();
	return points;
}

template<typename T>
Eigen::Matrix<T, 4, 4> gen_homogeneous_trafo(Eigen::Matrix<T, 3, 1> shift, Eigen::Matrix<T, 3, 3> rotation) {
	/*
	Get homogeneous matrix from shift vector and rotation matrix.

	Combines a given shift vector and a given rotation matrix to a homogeneous
	transformation matrix and returns the result.

	:param shift: The shift vector to use given as 3 element numpy array.
	:param rotation: The rotation matrix to use given as 3x3 numpy array.
	:return: The transformation matrix generated from shift vector and rotation
	     matrix as 4x4 numpy array.
	*/
	//std::cout << "gen homogen traf" << std::endl;
	Eigen::Matrix<T, 4, 4> sol;
	sol.block(0, 0, 3, 3) = rotation;
	sol.block(0, 3, 3, 1) = shift;
	sol(3,0) = 0;
	sol(3,1) = 0;
	sol(3,2) = 0;
	sol(3,3) = 1;
	return sol;
}

inline double area(std::pair<double, double> x, std::pair<double, double> y, std::pair<double, double> z);

std::vector<std::vector<int>> triangulate(std::vector<int> iV, std::vector<Eigen::Matrix<double, 3, 1>> V, Eigen::Matrix<double, 3, 1> N);

template<typename T>
int add_elem(std::vector<T> &L, T x) {
	L.push_back(x);
	return L.size() - 1;
}
#endif
