#include "Robot.h"
#include "fwdkin.h"
#include "utils.h"
#include "Mesh.h"

MeshRobot::MeshRobot() {
}

MeshRobot::MeshRobot(std::vector<std::vector<double>> dh_table, std::vector<double> joint_types,
		std::vector<double> pose, std::vector<Mesh> menclosures,
		Eigen::Matrix<double, 3, 1> position, Eigen::Matrix<double, 3, 3> orientation) {
	this->dh_table = dh_table;
	this->joint_types = joint_types;
	this->menclosures = menclosures;
	for(auto m : menclosures) link_empty.push_back(int(m.verts.size() < 4));
	for(int i = 0; i < int(menclosures.size()); i ++) 
		if(!link_empty[i]) joint_list.push_back(i);
	num_links = pose.size() + 1;
	_base_to_world_trafo = gen_homogeneous_trafo(position, orientation);
	_init_mesh_geometry();
	Eigen::Matrix<double, Eigen::Dynamic, 1> epose(pose.size(), 1);
	for(int i = 0; i < int(pose.size()); i ++) epose(i, 0) = pose[i];
	update_pose(epose);
	coll_req.enable_contact = false;
	coll_req.enable_cost = false;
}

MeshRobot::~MeshRobot() {
}

void MeshRobot::update_pose(std::vector<double> pose) {
	Eigen::Matrix<double, -1, 1> epose(pose.size(), 1);
	for(int i = 0; i < int(pose.size()); i ++) epose(i, 0) = pose[i];
	update_pose(epose);
}

void MeshRobot::update_pose(Eigen::Matrix<double, Eigen::Dynamic, 1> pose) {
	this->pose = pose;
        _calc_trafos_to_world();
	for(int i = 0; i < int(menclosures.size()); i ++) {
		if(link_empty[i]) continue;
		collision_objects[i].setTransform(_to_world_trafos[i].block<3,3>(0,0), 
			_to_world_trafos[i].block<3,1>(0,3)); 
	}
}

void MeshRobot::_init_mesh_geometry() {
        //Initialize geometric representation of the robot.
	int n = menclosures.size();
        collision_geometries.clear();
	collision_objects.clear();
	for(int i = 0; i < n; i ++) {
		auto mesh = menclosures[i];
		std::shared_ptr<fcl::BVHModel<fcl::OBBRSS<double>>> mesh_obj = std::make_shared<fcl::BVHModel<fcl::OBBRSS<double>>>();
		if(!link_empty[i]) {
			mesh_obj->beginModel();
			std::vector<fcl::Triangle> faces;
			for(auto face : mesh.faces) faces.push_back({std::size_t(face[0]), 
							std::size_t(face[1]), std::size_t(face[2])});
			mesh_obj->addSubModel(mesh.verts, faces);
			mesh_obj->endModel();
		}
		collision_geometries.push_back(mesh_obj);
		collision_objects.push_back(fcl::CollisionObject<double>(collision_geometries.back()));
	}
}

void MeshRobot::_calc_trafos_to_world() {
        //Calculate transformations form link coordinates to world coordinates.
        _to_world_trafos = fwdkin_hom_full(pose, dh_table, joint_types, 
						_base_to_world_trafo);
}

bool MeshRobot::check_self_collision() {
        //Check for self collision ignoring collisions of adjacent links.
        bool res = false;
	for(int k = 0; k < int(joint_list.size()); k ++) {
		for(int l = 0; l < k - 1; l ++) {
			int i = joint_list[k];
			int j = joint_list[l];
			fcl::CollisionResult<double> coll_res = fcl::CollisionResult<double>();
			bool res = fcl::collide(&collision_objects[i], &collision_objects[j], coll_req , coll_res);
			if(coll_res.isCollision()) return res;
		}
	}
        return res;
}

bool MeshRobot::check_collision(fcl::CollisionObject<double>* obstacle) {
        //Check for collision with an obstacle.
        bool res = false;
	for(int i : joint_list) {
		fcl::CollisionResult<double> coll_res = fcl::CollisionResult<double>();
		bool res = fcl::collide(&collision_objects[i], obstacle,
				  coll_req, coll_res);
		if(coll_res.isCollision()) return res;
	}
        return res;
}

bool MeshRobot::check_collision(std::vector<fcl::CollisionObject<double>> &obstacle) {
        //Check for collision with an obstacle.
        bool res = false;
	for(int i : joint_list) {
		for(auto obj : obstacle) {
			fcl::CollisionResult<double> coll_res = fcl::CollisionResult<double>();
			bool res = fcl::collide(&collision_objects[i], &obj,
					  coll_req, coll_res);
			if(coll_res.isCollision()) return res;
		}
	}
        return res;
}
void MeshRobot::saveTo(std::string filename,
		Eigen::Matrix<double, Eigen::Dynamic, 1> pose) {
	Mesh* m = getMesh(pose);
	//m->print();
	m->saveTo(filename);
}

Mesh* MeshRobot::getMesh(Eigen::Matrix<double, Eigen::Dynamic, 1> pose) {
	this->pose = pose;
        _calc_trafos_to_world();
	Mesh* full = new Mesh();
	Mesh temp;
	for(int i = 0; i < int(menclosures.size()); i ++) {
		if(link_empty[i]) continue;
		temp = menclosures[i];
		temp.setTransformation(_to_world_trafos[i]);
		temp.applyStoredTransformation();
		full->merge(&temp);
	}
	return full;
}
