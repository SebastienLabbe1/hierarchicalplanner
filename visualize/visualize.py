from mpl_toolkits import mplot3d
import numpy as np
import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d.art3d as art3d
from matplotlib.patches import Rectangle
import sys

pname = sys.argv[1]

DATA = "data/"
ROBOT = "robots/"
ENV = "environments/"
PROB = "problems/"
STLFILE = "stlfiles/"

folder = DATA + PROB + pname + "/"

x = []
y = []
z = []
with open(folder + "solution.txt") as file:
	for line in file.readlines():
		line = [float(xx) for xx in line.split()]
		#print(line)
		x.append(line[0])
		y.append(line[1])
		z.append(line[2])

ax = plt.axes(projection='3d')

mov = []

with open(folder + "movements.txt") as file:
	tx = []
	ty = []
	tz = []
	for line in file.readlines():
		if line[0] == "#":
			tx = []
			ty = []
			tz = []
			mov.append((tx, ty, tz))
			continue
		line = [float(xx) for xx in line.split()]
		#print(line)
		tx.append(line[0])
		ty.append(line[1])
		tz.append(line[2])
mov = mov[:-1]
	
n = 5
N = len(mov)

toshow = [int((N -1) * i / n) for i in range(n + 1)]	

for i in toshow:
	ax.plot3D(mov[i][0], mov[i][1], mov[i][2], 'red')


minx = 10000000
maxx = -10000000
miny = 10000000
maxy = -10000000
minz = 10000000
maxz = -10000000
with open(folder + "bounds.txt") as file:
	for line in file.readlines():
		cx, cy, cz, dx, dy, dz = [float(xix) for xix in line.split()]
		minx = min(cx, minx)
		miny = min(cy, miny)
		minz = min(cz, minz)
		maxx = max(cx + dx, maxx)
		maxy = max(cy + dy, maxy)
		maxz = max(cz + dz, maxz)
		p = Rectangle((cx, cy), dx, dy, color='green', fill=False)
		ax.add_patch(p)
		art3d.pathpatch_2d_to_3d(p, z=cz, zdir="z")
		p = Rectangle((cx, cy), dx, dy, color='green', fill=False)
		ax.add_patch(p)
		art3d.pathpatch_2d_to_3d(p, z=cz + dz, zdir="z")

		p = Rectangle((cx, cz), dx, dz, color='green', fill=False)
		ax.add_patch(p)
		art3d.pathpatch_2d_to_3d(p, z=cy, zdir="y")
		p = Rectangle((cx, cz), dx, dz, color='green', fill=False)
		ax.add_patch(p)
		art3d.pathpatch_2d_to_3d(p, z=cy + dy, zdir="y")

		p = Rectangle((cy, cz), dy, dz, color='green', fill=False)
		ax.add_patch(p)
		art3d.pathpatch_2d_to_3d(p, z=cx, zdir="x")
		p = Rectangle((cy, cz), dy, dz, color='green', fill=False)
		ax.add_patch(p)
		art3d.pathpatch_2d_to_3d(p, z=cx + dx, zdir="x")

# Data for a three-dimensional line
#print(x, y, z)
#ax.plot3D(x, y, z, 'green')
ax.plot3D(x, y, z)
ax.set_xlabel('x')
ax.set_ylabel('y')
ax.set_zlabel('z')
axes = plt.gca()
axes.set_xlim([minx,maxx])
axes.set_ylim([miny,maxy])
axes.set_zlim([minz,maxz])

# Data for three-dimensional scattered points
#ax.scatter3D(xdata, ydata, zdata, c=zdata, cmap='Greens');
plt.show()


