import faster_visualizer as fv
import utils as u
import numpy, math, colormap, sys
VIS_FOLDER = "../visualize/vis/"
TABLES_FOLDER = "../visualize/tables/"

def printLengths(filenames):
	for filename in filenames:
		print(filename)
		length = 0
		x, xcount = u.loadcsv(VIS_FOLDER + filename + ".csv")
		for a,b in zip(x[:-1], x[1:]):
			length += numpy.linalg.norm(a-b)
		print("carthesian space length :", length)
		length = 0
		s, scount = u.loadcsv(VIS_FOLDER + filename + "state.csv")
		for a,b in zip(s[:-1], s[1:]):
			length += numpy.linalg.norm(a-b)
		print("joint space length :", length)
		t, tcount = u.loadcsv(VIS_FOLDER + filename + "time.csv")
		print("Solver setup time : ", t[0])
		print("Solver solve time : ", t[1])
		print("Solve probability : ", t[2])

def saveLengths(filenames, probname):
	with open(TABLES_FOLDER + probname + ".txt",'w') as file:
		for filename in filenames:
			t, tcount = u.loadcsv(VIS_FOLDER + filename + "time.csv")
			file.write(filename + "\n")
			file.write(str(t[0]) + "\n")
			file.write(str(t[1]) + "\n")
			length = 0
			s, scount = u.loadcsv(VIS_FOLDER + filename + "state.csv")
			for a,b in zip(s[:-1], s[1:]):
				length += numpy.linalg.norm(a-b)
			file.write(str(length) + "\n")
			length = 0
			x, xcount = u.loadcsv(VIS_FOLDER + filename + ".csv")
			for a,b in zip(x[:-1], x[1:]):
				length += numpy.linalg.norm(a-b)
			file.write(str(length) + "\n")
			file.write(str(t[2]) + "\n")



saveLengths(["hpath", "spath", "rpath", "bpath"], sys.argv[1])
printLengths(["hpath", "spath", "rpath", "bpath"])

