import faster_visualizer as fv
import utils as u
import numpy, math, colormap
FOLDER = "../visualize/vis/"
hexcolors = ["#000000", "#FFFF00", "#FF4A46", "#FF34FF", "#1CE6FF", "#008941", "#006FA6", "#A30059",
        "#FFDBE5", "#7A4900", "#0000A6", "#63FFAC", "#B79762", "#004D43", "#8FB0FF", "#997D87",
        "#5A0007", "#809693", "#FEFFE6", "#1B4400", "#4FC601", "#3B5DFF", "#4A3B53", "#FF2F80",
        "#61615A", "#BA0900", "#6B7900", "#00C2A0", "#FFAA92", "#FF90C9", "#B903AA", "#D16100",
        "#DDEFFF", "#000035", "#7B4F4B", "#A1C299", "#300018", "#0AA6D8", "#013349", "#00846F",
        "#372101", "#FFB500", "#C2FFED", "#A079BF", "#CC0744", "#C0B9B2", "#C2FF99", "#001E09",
        "#00489C", "#6F0062", "#0CBD66", "#EEC3FF", "#456D75", "#B77B68", "#7A87A1", "#788D66",
        "#885578", "#FAD09F", "#FF8A9A", "#D157A0", "#BEC459", "#456648", "#0086ED", "#886F4C",

        "#34362D", "#B4A8BD", "#00A6AA", "#452C2C", "#636375", "#A3C8C9", "#FF913F", "#938A81",
        "#575329", "#00FECF", "#B05B6F", "#8CD0FF", "#3B9700", "#04F757", "#C8A1A1", "#1E6E00",
        "#7900D7", "#A77500", "#6367A9", "#A05837", "#6B002C", "#772600", "#D790FF", "#9B9700",
        "#549E79", "#FFF69F", "#201625", "#72418F", "#BC23FF", "#99ADC0", "#3A2465", "#922329",
        "#5B4534", "#FDE8DC", "#404E55", "#0089A3", "#CB7E98", "#A4E804", "#324E72", "#6A3A4C",
        "#83AB58", "#001C1E", "#D1F7CE", "#004B28", "#C8D0F6", "#A3A489", "#806C66", "#222800",
        "#BF5650", "#E83000", "#66796D", "#DA007C", "#FF1A59", "#8ADBB4", "#1E0200", "#5B4E51",
        "#C895C5", "#320033", "#FF6832", "#66E1D3", "#CFCDAC", "#D0AC94", "#7ED379", "#012C58"]
tupcolors = [[rgbc/255.0 for rgbc in colormap.hex2rgb(hc)] for hc in hexcolors]


def show(meshes, lines):
	vis = fv.FasterVisualizer()
	if not len(meshes):
		vs, fs = u.gen_mesh_cube(numpy.array([0.1, 0.1, 0.1]), 0.1)
		vis.add_mesh(vs, fs)
	for i,filename in enumerate(meshes):
		fs, nfs = u.loadcsv(FOLDER + filename + "f.csv", numpy.int32)
		vs, nvs = u.loadcsv(FOLDER + filename + "v.csv")
		color = (tupcolors[i][0], tupcolors[i][1], tupcolors[i][2], 0.6)
		#print(vs, fs)
		#print(filename)
		if(len(vs) == 0):
			continue
		vis.add_mesh(vs, fs, color)
	for i,filename in enumerate(lines):
		j = i + len(meshes)
		ls, nls = u.loadcsv(FOLDER + filename+".csv")
		color = (tupcolors[j][0], tupcolors[j][1], tupcolors[j][2], 0.6)
		vis.add_line(ls,color)
	vis.show_plot()
	
show([], ["workspacel"])
show(["obstacles"], [])
show(["hpathint"], [])
show(["obstacles", "hpathint"], ["hpath"])
show(["obstacles", "spathint"], ["spath"])
show(["obstacles", "rpathint"], ["rpath"])
#show(["obstacles", "bpathint"], ["bpath"])

exit()
vis = fv.FasterVisualizer()
nf,nfs = u.loadcsv("vis/envf.csv")
for i in range(int(nf)):
	#print(" i " , i )
	vs, nvs = u.loadcsv("vis/envv.csv")
	fs, nfs = u.loadcsv("vis/envf{}.csv".format(i), numpy.int32)
	#print(fs, vs)
	color = (1, 0.5, 0.5, 0.8)
	vis.add_mesh(vs, fs, color)
	vis.show_plot()
	vis.clear()
exit()
for i in range(int(nf)):
	#print(" i " , i )
	vs, nvs = u.loadcsv("envv.csv")
	fs, nfs = u.loadcsv("envf{}.csv".format(i), numpy.int32)
	#print(fs, vs)
	for j,f in enumerate(fs):
		#print(" j " , j)
		#print(j, f, nf, i, nfs)
		color = (float(j) / nfs[0], 
			1.0 - float(j) / nfs[0], 
			1.0 - float(j) / nfs[0], 
			0.8)
		#print(color)
		tf = [[0, 1, 2]] 
		tv = [vs[fi] for fi in f]
		#print(tv)
		vis.add_mesh(tv, tf, color)
	vis.show_plot()
	vis.clear()

exit()
for i in range(int(nf)):
	#print(" i " , i )
	vs, nvs = u.loadcsv("vis/envv.csv")
	fs, nfs = u.loadcsv("vis/envf{}.csv".format(i), numpy.int32)
	#print(fs, vs)
	for j,f in enumerate(fs):
		#print(" j " , j)
		#print(j, f, nf, i, nfs)
		color = (float(j) / nfs[0], 
			1.0 - float(j) / nfs[0], 
			1.0 - float(j) / nfs[0], 
			0.8)
		#print(color)
		tf = [[0, 1, 2]] 
		tv = [vs[fi] for fi in f]
		#print(tv)
		vis.add_mesh(tv, tf, color)
vis.show_plot()


