#This code was obtained from Constantin Dresel

"""A module for fast 3d visualization of triangle meshes using vispy."""
import numpy
import vispy.plot


class FasterVisualizer:
    """A class for visualizing meshes in 3d using vispy."""

    def __init__(self):
        """Constructor: Initialize an empty 3d plot for visualization."""
        self.fig = vispy.plot.Fig(show=False)
        self.axes = self.fig[0, 0]
        self.mesh_verts_list = []
        self.mesh_faces_list = []
        self.mesh_color_list = [] 
        self.lines = []
        self.line_colors = []

    def clear(self):
        """Clear the plot to initial values"""
        self.fig = vispy.plot.Fig(show=False)
        self.axes = self.fig[0, 0]
        self.mesh_verts_list = []
        self.mesh_faces_list = []
        self.mesh_color_list = []
        self.lines = []
        self.line_colors = []

    def add_mesh(self, vects, faces, rgba_color=(.5, .5, .5, 1.0)):
        """Add a triangle mesh to the visualization.

        :param vects: A duplicate-free array of vertices of the mesh. Shape Nx3.
        :param faces: Array containing the indices of triangle faces in the
                      mesh. Shape Nx3 - one triangle definition per row.
        :param rgba_color: A four element tuple defining the mesh color in rgba.
        :returns: None.
        """
        self.mesh_verts_list.append(vects)
        self.mesh_faces_list.append(faces)
        self.mesh_color_list.append(rgba_color)

    def add_mesh_list(self, vects, faces, rgba_color=(.5, .5, .5, 1.0)):
        """Add multiple triangle meshes to the visualization.

        Hand multiple vertex arrays and face arrays to this method for
        visualization as one list of vertex arrays and one list of face arrays.
        Both lists should have the same number of elements. Each element of each
        of the lists should have the shape Nx3 as for add_mesh().

        :param vects: A list of duplicate-free arrays of vertices of the meshes.
        :param faces: A list of 2D arrays containing the indices of triangle
                      faces in the meshes.
        :param rgba_color: A four element tuple defining the color for the
                           meshes in rgba.
        :returns: None.
        """
        assert len(vects) == len(faces), "Lengths of lists to add differ!"
        self.mesh_verts_list += vects
        self.mesh_faces_list += faces
        self.mesh_color_list += ([rgba_color] * len(vects))

    def add_line(self, line, color=(.5, .5, .5, 1.0)):
        self.lines.append(line)
        self.line_colors.append(color)

    def show_plot(self):
        """Show the plot containing all previously added meshes on screen.

        This method will not be left until the visualization window gets closed.

        :returns: None.
        """
        for mesh_verts, mesh_faces, mesh_color in zip(self.mesh_verts_list,
                                                      self.mesh_faces_list,
                                                      self.mesh_color_list):
            self.axes.mesh(numpy.ascontiguousarray(mesh_verts),
                           numpy.ascontiguousarray(mesh_faces),
                           color=mesh_color)
        for line, line_color in zip(self.lines, self.line_colors):
            self.axes.plot(line, color=line_color)
        self.fig.show(run=True)
