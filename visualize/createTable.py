import sys
TABLES_FOLDER = "../visualize/tables/"
filename = sys.argv[1] + ".txt"
table = [([0] * 4) for i in range(6)]
h = """\\begin{table}[h!] 
	\\begin{center}
		\caption{Problem title results}
		\label{tab:tabletitle}
		\\begin{tabular}{|l|r|r|r|r|}
			\hline
			& \\textbf{Hierarchical} & \\textbf{RRTstar} & \\textbf{RRTconnect} & \\textbf{RRTstar (control)}\\\\
			\hline
			setup (s)               & 0x0 & 0x1 & 0x2 & 0x3\\\\
			\hline
			solve (s)               & 1x0 & 1x1 & 1x2 & 1x3\\\\
			\hline
			total (s)               & 2x0 & 2x1 & 2x2 & 2x3\\\\
			\hline
			path length (joint)     & 3x0 & 3x1 & 3x2 & 3x3\\\\
			\hline
			path length (cartesian) & 4x0 & 4x1 & 4x2 & 4x3\\\\
			\hline
			solve chance (\%)       & 5x0 & 5x1 & 5x2 & 5x3\\\\
			\hline
		\end{tabular}
	\end{center}
\end{table}"""
with open(TABLES_FOLDER + filename) as file:
	r = 0
	c = -1
	for line in file.readlines():
		line = line.strip()
		if line[1:] == "path":
			r = 0
			c += 1
			continue
		v = float(line)
		table[r][c] = v
		if r == 1:
			table[2][c] = table[0][c] + table[1][c]
			r += 1
		r += 1
print(table)
for i in range(4):
	for j in range(6):
		number = table[j][i]
		if j == 5:
			number *= 100
			h = h.replace(str(j)+"x"+str(i), f'{number:9.2f}')
		else:
			h = h.replace(str(j)+"x"+str(i), f'{number:9.6f}')
h = h.replace("title", sys.argv[1])

with open("../visualize/tex/"+sys.argv[1]+"table.tex","w") as file:
	file.write(h)
