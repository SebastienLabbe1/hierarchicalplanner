#This code was obtained from Constantin Dresel

"""A module comprising various helper functions and definitions."""
import numpy
import stl

#from fwdkin import fwdkin, fwdkin_hom


APPROX_ZERO = .00000000000001
"""A constant to use for checking abs of floating point values against zero."""

SCALE_MILLIMETER_TO_SI = 1.0/1000.0
"""A scaling factor to convert millimeters to meters - i.e. to SI units."""

BOX_TRI_INDICES = numpy.array([[1, 2, 3],
                               [0, 1, 3],
                               [0, 3, 4],
                               [0, 1, 4],
                               [1, 4, 5],
                               [1, 2, 5],
                               [2, 5, 6],
                               [2, 3, 6],
                               [3, 6, 7],
                               [5, 6, 7],
                               [4, 5, 7],
                               [3, 4, 7]], dtype=int)
"""Triangle corner indices for a box mesh."""

OCTAHEDRON_TRI_INDICES = numpy.array([[0, 2, 5],
                                      [2, 5, 1],
                                      [5, 1, 3],
                                      [5, 3, 0],
                                      [3, 0, 4],
                                      [0, 4, 2],
                                      [4, 2, 1],
                                      [4, 1, 3]], dtype=int)
"""Triangle corner indices for an octahedron mesh."""

ARROW_TRI_INDICES = numpy.array([[0, 1, 2],  # vertical body
                                 [2, 1, 3],
                                 [7, 8, 9],  # horizontal body
                                 [9, 8, 10],
                                 [4, 5, 6],  # vertical tip
                                 [11, 12, 6]  # horizontal tip
                                 ], dtype=int)
"""Triangle corner indices for an arrow shaped mesh."""


def loadcsv(csvpath, data_type=numpy.float64):
    """Load data from a specified csv-file and return data and array size.

    Loads data form a csv-file at the specified path as numpy ndarray.
    Additionally estimates the dimensions of the loaded array and puts them in a
    second 1D numpy ndarray of data type int32.

    :param csvpath: Path to the csv file to load
    :param data_type: Data type for the array data are read into as numpy type.
    :return: The parsed array and a 2nd array giving size of the parsed array.
    """
    data = numpy.loadtxt(csvpath, delimiter=',', dtype=data_type)
    size = numpy.asarray(data.shape, dtype=numpy.int32)
    return data, size

def stl_to_npy_file(filename):
    """ Generate and store mesh as vertex array and face index array from stl.

    Generate a duplicate-free numpy array of vertices (shape Nx3) and a numpy
    array containing vertex indices for triangle faces (shape Mx3) from a given
    stl-file and store the generated arrays to one file each. Storage location
    is the same as the location of the used stl-file.

    :param filename: Path to the stl-file to load and convert to arrays.
    :return: None.
    """
    stl_mesh = stl.mesh.Mesh.from_file(filename)
    stl_vects = stl_mesh.vectors
    vectors = stl_vects.reshape(stl_vects.shape[0] * stl_vects.shape[1],
                                stl_vects.shape[2])
    # deduplicate vertices
    vectors_dedup = numpy.unique(vectors, axis=0)
    # find face indices
    faces_flat = numpy.empty(vectors.shape[0], dtype=int)
    for idx, val in enumerate(vectors_dedup):
        min_ok = (vectors >= val - APPROX_ZERO).all(axis=1)
        max_ok = (vectors <= val + APPROX_ZERO).all(axis=1)
        res = min_ok * max_ok
        faces_flat[res] = idx
    faces = numpy.reshape(faces_flat, (int(vectors.shape[0]/vectors.shape[1]
                                           ), vectors.shape[1]))
    # save results to file
    numpy.save(filename[:-4] + "_faces", faces)
    numpy.save(filename[:-4] + "_vecs", vectors_dedup)


def gen_mesh_cube(center, half_len):
    """Generate a triangle mesh for a cube with given position and size.

    Generates a vertex array and a face array. The vertex array is a
    duplicate-free representation of the vertices of the triangle mesh - shape:
    Nx3. The face array contains the indices of the corners of the triangle
    faces of the mesh in the vertex array - shape: Mx3.

    :param center: The center of the cube.
    :param half_len: Half the length of an edge of the cube.
    :return: A vertex array transformed to realworld and an array with
             associated triangle face indices.
    """
    corners = numpy.empty([8, 3], dtype=float)
    corners[0] = center - [-half_len, -half_len, -half_len]
    corners[1] = center - [-half_len, -half_len, half_len]
    corners[2] = center - [half_len, -half_len, half_len]
    corners[3] = center - [half_len, -half_len, -half_len]
    corners[4] = center - [-half_len, half_len, -half_len]
    corners[5] = center - [-half_len, half_len, half_len]
    corners[6] = center - [half_len, half_len, half_len]
    corners[7] = center - [half_len, half_len, -half_len]
    return corners, BOX_TRI_INDICES


def gen_mesh_arrow_x(origin, len_to_tip):
    """Generate a triangle mesh for an arrow pointing to x-direction.

    Generates a vertex array and a face array. The vertex array is a
    duplicate-free representation of the vertices of the triangle mesh - shape:
    Nx3. The face array contains the indices of the corners of the triangle
    faces of the mesh in the vertex array - shape: Mx3.

    :param origin: The origin of the arrow.
    :param len_to_tip: The total length of the arrow from origin to tip.
    :return: A vertex array transformed to realworld and an array with
             associated triangle face indices.
    """
    origin = numpy.asarray(origin, dtype=float)
    tip = origin + [len_to_tip, .0, .0]
    corners = numpy.empty([13, 3], dtype=float)
    # body - vertical
    corners[0] = origin + [.0, .0, -.05 * len_to_tip]
    corners[1] = origin + [.0, .0, .05 * len_to_tip]
    corners[2] = origin + .8 * (tip - origin) + [.0, .0, -.05 * len_to_tip]
    corners[3] = origin + .8 * (tip - origin) + [.0, .0, .05 * len_to_tip]
    # tip - vertical
    corners[4] = origin + .8 * (tip - origin) + [.0, .0, -0.09 * len_to_tip]
    corners[5] = origin + .8 * (tip - origin) + [.0, .0, 0.09 * len_to_tip]
    corners[6] = tip
    # body - horizontal
    corners[7] = origin + [.0, -.05 * len_to_tip, .0]
    corners[8] = origin + [.0, .05 * len_to_tip, .0]
    corners[9] = origin + .8 * (tip - origin) + [.0, -.05 * len_to_tip, .0]
    corners[10] = origin + .8 * (tip - origin) + [.0, .05 * len_to_tip, .0]
    # tip - horizontal
    corners[11] = origin + .8 * (tip - origin) + [.0, -0.09 * len_to_tip, .0]
    corners[12] = origin + .8 * (tip - origin) + [.0, 0.09 * len_to_tip, .0]
    return corners, ARROW_TRI_INDICES


def gen_mesh_arrow_y(origin, len_to_tip):
    """Generate a triangle mesh for an arrow pointing to y-direction.

    Generates a vertex array and a face array. The vertex array is a
    duplicate-free representation of the vertices of the triangle mesh - shape:
    Nx3. The face array contains the indices of the corners of the triangle
    faces of the mesh in the vertex array - shape: Mx3.

    :param origin: The origin of the arrow.
    :param len_to_tip: The total length of the arrow from origin to tip.
    :return: A vertex array transformed to realworld and an array with
             associated triangle face indices.
    """
    rot_matrix = numpy.array([[.0, 1.0, .0], [1.0, .0, 0.], [.0, .0, 1.0]])
    verts, tris = gen_mesh_arrow_x(rot_matrix.dot(origin), len_to_tip)
    for vert_idx in range(len(verts)):
        verts[vert_idx] = rot_matrix.dot(verts[vert_idx])
    return verts, tris


def gen_mesh_arrow_z(origin, len_to_tip):
    """Generate a triangle mesh for an arrow pointing to z-direction.

    Generates a vertex array and a face array. The vertex array is a
    duplicate-free representation of the vertices of the triangle mesh - shape:
    Nx3. The face array contains the indices of the corners of the triangle
    faces of the mesh in the vertex array - shape: Mx3.

    :param origin: The origin of the arrow.
    :param len_to_tip: The total length of the arrow from origin to tip.
    :return: A vertex array transformed to realworld and an array with
             associated triangle face indices.
    """
    rot_matrix = numpy.array([[.0, .0, 1.0], [.0, 1.0, 0.], [1.0, .0, .0]])
    verts, tris = gen_mesh_arrow_x(rot_matrix.dot(origin), len_to_tip)
    for vert_idx in range(len(verts)):
        verts[vert_idx] = rot_matrix.dot(verts[vert_idx])
    return verts, tris


def byte_to_signed_int8(byte):
    """Convert a byte (unsigned 0 to 255) to a signed 8bit int representation.

    Converts a byte value - i.e. a positive integer between 0 and 255 - to the
    corresponding signed integer value if the byte was interpreted as 8bit
    integer.

    :param byte: A non-negative integer representing the byte to convert.
    :return: The signed integer representation of the given byte value
             interpreted as 8bit integer.
    """
    # heavily inspired by https://stackoverflow.com/a/9147327 (20.12.2018)
    res = byte
    if res & 0x80:  # if sign bit set
        res = res - 0x100
    return res
